/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                                            */
/* Date: 06-Mar-19                                                */
/* Version: 01                                                */
/* Description: Configuration file for SYSTICK Driver   */
/**************************************************************/

/*Preprocessor Guard*/
#ifndef SYSTICK_CONFIG_H 
#define SYSTICK_CONFIG_H 

/* Macro to define the AHB clock frequency which is the source of Systick Timer */
#define SYSTICK_u8_AHB_FREQ   (u32)8000000

/*
 Macro to select the clock source of the Systick Timer
 Ranges:	SYSTICK_u8_AHB
		    SYSTICK_u8_AHB_OVER_8
*/
#define SYSTICK_u8_CLKSOURCE  SYSTICK_u8_AHB_OVER_8

/*
 Macro to select the initialized state of Systick Timer Interrupt
 Ranges:	SYSTICK_u8_ENABLE_EXEP_REQ
		    SYSTICK_u8_DISABLE_EXEP_REQ
*/
#define SYSTICK_u8_TICKINT    SYSTICK_u8_ENABLE_EXEP_REQ


#endif /* SYSTICK_CONFIG_H */ 
