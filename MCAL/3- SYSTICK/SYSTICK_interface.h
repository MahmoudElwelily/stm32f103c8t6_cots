/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                                        */
/* Date: 06-Mar-19                                            */
/* Version: 01                                            */
/* Description: Interface file for SYSTICK Driver    */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef SYSTICK_INTERFACE_H 
#define SYSTICK_INTERFACE_H 

/*Macro to define a pointer to function as a specific datatype */
typedef void (*CallBack_t) (void);

/*
 * Description: Function to set the initialized configuration in the SysTick control and status register (STK_CTRL)
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vInit(void);

/*
 * Description: Function to enable Systick Timer
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vEnable (void);

/*
 * Description: Function to disable Systick Timer
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vDisable (void);

/*
 * Description: Function to set the time needed between each Systick Interrupt
 * Inputs: The needed time in millisecond
 * Output: void
 */
extern void SYSTICK_vSetTickTime (u16 Copy_TimeMs);

/*
 * Description: Function to set the initialized configuration in t
 * Inputs: none
 * Output: the Error state of the function
 */
extern void SYSTICK_vSetCallBack (CallBack_t Func);

/*
 * Description: Function to make delay in millisecond using the SYSTICK timer (without interrupt)
 * Inputs: the required delay as a multiple of 10ms (n*10ms)
 * Output: void
 */
extern void SysTick_Wait10ms(u32 Copy_u32Delay);

/*
 * Description: Function to make delay in nanosecond using the SYSTICK timer (without interrupt)
 * Inputs: the required delay as a multiple of 10ns (n*10ns)
 * Output: void
 */
extern void SysTick_Wait10us(u32 Copy_u32Delay);

#endif /* SYSTICK_INTERFACE_H */ 
