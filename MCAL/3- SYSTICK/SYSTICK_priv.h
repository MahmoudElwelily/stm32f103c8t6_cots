/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                          */
/* Date: 06-Mar-19                                        */
/* Version: 01                                            */
/* Description: Private file for SYSTICK Driver           */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef SYSTICK_PRIV_H 
#define SYSTICK_PRIV_H 

/*Macros for the possible clock frequencies that can feed the Systick Peripheral*/
#define SYSTICK_u8_AHB                  (u8)0
#define SYSTICK_u8_AHB_OVER_8           (u8)1

/*Macros for the possible states for the SysTick exception request*/
#define SYSTICK_u8_DISABLE_EXEP_REQ		(u8)0
#define SYSTICK_u8_ENABLE_EXEP_REQ      (u8)1

/*Macros for the required number of count should be made to make a delay
of 10ms or 10us in case of Systick source is AHB & the AHB freq is 8Mhz*/
#define SYSTICK_u8_10MS_COUNT_AHB           (u32)80000
#define SYSTICK_u8_10US_COUNT_AHB           (u32)80

/*Macros for the required number of count should be made to make a delay
of 10ms or 10us in case of Systick source is AHB/8 & the AHB freq is 8Mhz*/
#define SYSTICK_u8_10MS_COUNT_AHB_OVER_8	(u32)10000
#define SYSTICK_u8_10US_COUNT_AHB_OVER_8	(u32)10

typedef struct
{
	u32 STK_CTRL;
	u32 STK_LOAD;
	u32 STK_VAL;
	u32 STK_CALIB;
}SYSTICK;
#define SYSTICK_TIMER          ((SYSTICK*)0xE000E010)

/*Defining the STK_CTRL register bits */
#define ENABLE      (u8)0
#define TICKINT     (u8)1
#define CLKSOURCE   (u8)2
#define COUNTFLAG   (u8)16

/*
 * Description: The function required for making the delay
 * Inputs: the required number of count should be loaded in theSysTick reload value register (STK_LOAD)
 * Output: void
 */
static void SYSTICK_vSysTickWait(u32 Copy_u32Delay);

#endif /* SYSTICK_PRIV_H */ 
