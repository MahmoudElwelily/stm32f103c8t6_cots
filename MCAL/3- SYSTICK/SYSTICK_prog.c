/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                                            */
/* Date: 06-Mar-19                                                */
/* Version: 01                                                */
/* Description: Program file for SYSTICK Driver         */
/**************************************************************/
#include "STD_TYPES.h"
#include "BIT_CALC.h" 
#include "SYSTICK_interface.h" 
#include "SYSTICK_priv.h"
#include "SYSTICK_config.h"

static CallBack_t SYSTICK_CallbackPtrFun = NULL;

/*
 * Description: Function to set the initialized configuration in the SysTick control and status register (STK_CTRL)
 * Inputs: none
 * Output: void
 */
void SYSTICK_vInit(void)
{
	if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
	{
		SYSTICK_TIMER->STK_CTRL |= (1 << CLKSOURCE);
	}
	else if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB_OVER_8)
	{
		SYSTICK_TIMER->STK_CTRL &= ~(1 << CLKSOURCE);
	}

	if (SYSTICK_u8_TICKINT == SYSTICK_u8_DISABLE_EXEP_REQ)
	{
		SYSTICK_TIMER->STK_CTRL &= ~(1 << TICKINT);
	}
	else if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_ENABLE_EXEP_REQ)
	{
		SYSTICK_TIMER->STK_CTRL |= (1 << TICKINT);
	}
	return;
}

/*
 * Description: Function to enable Systick Timer
 * Inputs: none
 * Output: void
 */
void SYSTICK_vEnable (void)
{
	SYSTICK_TIMER->STK_CTRL |= (1 << ENABLE);
	return;
}

/*
 * Description: Function to disable Systick Timer
 * Inputs: none
 * Output: void
 */
void SYSTICK_vDisable (void)
{
	SYSTICK_TIMER->STK_CTRL &= ~(1 << ENABLE);
	return;
}

/*
 * Description: Function to set the time needed between each Systick Interrupt
 * Inputs: The needed time in millisecond
 * Output: void
 */
void SYSTICK_vSetTickTime (u16 Copy_TimeMs)
{
	f32 Local_f32TickTime = 0;

	/* if the Systick Timer takes the AHB clock frequency without prescaler */
	if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
	{
		/* The Systick Time in millisecond */
		Local_f32TickTime = (1.0/SYSTICK_u8_AHB_FREQ)*1000;
	}
	/* if the Systick Timer takes the AHB clock frequency over 8 */
	else
	{
		/* The Systick Time in millisecond */
		Local_f32TickTime = (8.0/SYSTICK_u8_AHB_FREQ)*1000;
	}
	SYSTICK_TIMER->STK_LOAD = (u32)(Copy_TimeMs/Local_f32TickTime);
	return;
}

/*
 * Description: Function to set the initialized configuration in t
 * Inputs: none
 * Output: the Error state of the function
 */
void SYSTICK_vSetCallBack (CallBack_t CallbackFunc)
{
	if (CallbackFunc != NULL)
	{
		SYSTICK_CallbackPtrFun = CallbackFunc;
	}
	return;
}

void SysTick_Handler (void)
{
	if (SYSTICK_CallbackPtrFun != NULL)
	{
		SYSTICK_CallbackPtrFun();
	}
	return;
}


/*
 * Description: Function to make delay in millisecond using the SYSTICK timer
 * Inputs: the required delay as a multiple of 10ms (n*10ms)
 * Output: void
 */
void SysTick_Wait10ms(u32 Copy_u32Delay)
{
	u8 i;
	for (i= 0 ; i < Copy_u32Delay ; i++)
	{
		if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
		{
			SYSTICK_vSysTickWait(SYSTICK_u8_10MS_COUNT_AHB);
		}
		/* if the Systick Timer takes the AHB clock frequency over 8 */
		else
		{
			SYSTICK_vSysTickWait(SYSTICK_u8_10MS_COUNT_AHB_OVER_8);
		}
	}
}

/*
 * Description: Function to make delay in nanosecond using the SYSTICK timer
 * Inputs: the required delay as a multiple of 10ns (n*10ns)
 * Output: void
 */
void SysTick_Wait10us(u32 Copy_u32Delay)
{
	u8 i;
	for (i= 0 ; i < Copy_u32Delay ; i++)
	{
		if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
		{
			SYSTICK_vSysTickWait(SYSTICK_u8_10US_COUNT_AHB);
		}
		/* if the Systick Timer takes the AHB clock frequency over 8 */
		else
		{
			SYSTICK_vSysTickWait(SYSTICK_u8_10US_COUNT_AHB_OVER_8);
		}
	}
}

/*
 * Description: The function required for making the delay
 * Inputs: the required number of count should be loaded in theSysTick reload value register (STK_LOAD)
 * Output: void
 */
static void SYSTICK_vSysTickWait(u32 Copy_u32Delay)
{
	SYSTICK_TIMER->STK_LOAD = Copy_u32Delay-1;
	/*Enable the Systick counter*/

	SYSTICK_TIMER->STK_VAL = 0;
	while(((SYSTICK_TIMER->STK_CTRL) & (1<<16)) == 0);
}

