/************************************************************/
/* Author: Mahmoud Alaa Elwelily                            */
/* Date: 24-Feb-19                                          */
/* Version: 02                                              */
/* Description: Interface file for RCC Driver in STM32F103  */
/************************************************************/

/*Preprocessor Guard*/
#ifndef RCC_INTERFACE_H 
#define RCC_INTERFACE_H 

/*Macros for APB2 peripherals*/
#define RCC_u8_TIM11             (u8)1
#define RCC_u8_TIM10             (u8)2
#define RCC_u8_TIM9              (u8)3
#define RCC_u8_ADC3              (u8)4
#define RCC_u8_USART1            (u8)5
#define RCC_u8_TIM8              (u8)6
#define RCC_u8_SPI1              (u8)7
#define RCC_u8_TIM1              (u8)8
#define RCC_u8_ADC2              (u8)9
#define RCC_u8_ADC1              (u8)10
#define RCC_u8_IOPD              (u8)11
#define RCC_u8_IOPC              (u8)12
#define RCC_u8_IOPB              (u8)13
#define RCC_u8_IOPA              (u8)14
#define RCC_u8_AFIO              (u8)15

/*Macros for APB1 peripherals*/
#define RCC_u8_DAC               (u8)16
#define RCC_u8_PWR               (u8)17
#define RCC_u8_BKP               (u8)18
#define RCC_u8_CAN               (u8)19
#define RCC_u8_I2C2              (u8)20
#define RCC_u8_I2C1              (u8)21
#define RCC_u8_UART5             (u8)22
#define RCC_u8_UART4             (u8)23
#define RCC_u8_USART3            (u8)24
#define RCC_u8_USART2            (u8)25
#define RCC_u8_SPI3              (u8)26
#define RCC_u8_SPI2              (u8)27
#define RCC_u8_WWDG              (u8)28
#define RCC_u8_TIM14             (u8)29
#define RCC_u8_TIM13             (u8)30
#define RCC_u8_TIM12             (u8)31
#define RCC_u8_TIM7              (u8)32
#define RCC_u8_TIM6              (u8)33
#define RCC_u8_TIM5              (u8)34
#define RCC_u8_TIM4              (u8)35
#define RCC_u8_TIM3              (u8)36
#define RCC_u8_TIM2              (u8)37

/*Macros for AHB peripherals*/
#define RCC_u8_SDIO              (u8)38
#define RCC_u8_FSMC              (u8)39
#define RCC_u8_CRC               (u8)40
#define RCC_u8_FLITF             (u8)41
#define RCC_u8_SRAM              (u8)42
#define RCC_u8_DMA2              (u8)43
#define RCC_u8_DMA1              (u8)44

/*Macros for the peripheral probable states (Enabled/Disabled)*/
#define RCC_u8_PERI_ENABLE      (u8)0
#define RCC_u8_PERI_DISABLE     (u8)1

/*
 * Description: Function to initialize Clock source and state (with prescaler or not)
 * Inputs: none
 * Output: void
 */
void RCC_vInit (void);

/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: the peripheral name
 * Output: Error state
 */
u8 RCC_vEnablePeripheralClock (u8 Copy_u8PeriName);

/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: the peripheral name
 * Output: Error state
 */
u8 RCC_vDisablePeripheralClock (u8 Copy_u8PeriName);

#endif /* RCC_INTERFACE_H */ 
