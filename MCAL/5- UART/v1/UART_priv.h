/****************************************************************/
/* Author: Mahmoud Alaa Elwelily                                */
/* Date: 27-Apr-19                                              */
/* Version: 01                                                  */
/* Description: Private file for UART Driver for STM32F103x     */
/****************************************************************/

/*Preprocessor Guard*/
#ifndef UART_PRIV_H 
#define UART_PRIV_H 

/*Private macros to define the possible states of UART Transmitter,Receiver and the interrupts*/
#define UART_ENABLE                   1
#define UART_DISABLE 				 0

/*Private macros to define the possible parity types*/
#define UART_u8_NO_PARITY                (u8)0
#define UART_u8_EVEN_PARITY              (u8)1
#define UART_u8_ODD_PARITY               (u8)2

/*Private macros to define the number of stop bits and the data size*/
#define UART_u8_EIGHT_BITS               (u8)0
#define UART_u8_NINE_BITS                (u8)1

#define UART_u8_HALF_BIT                 (u8)0
#define UART_u8_ONE_BIT                  (u8)1
#define UART_u8_ONE_AND_HALF_BIT         (u8)2
#define UART_u8_TWO_BITS				 (u8)3
/****************************************************************/
/* Defining UART regisiters                                     */
/****************************************************************/
#define  UART_NO_OF_CHANNEL       3
#define  UART1                    0
#define  UART2		    		  1
#define  UART3					  2
#define  USART1_BASE_ADDRESS      ((USART_REG*)0x40013800)
#define  USART2_BASE_ADDRESS      ((USART_REG*)0x40004400)
#define  USART3_BASE_ADDRESS      ((USART_REG*)0x40004800)

typedef struct
{
	u32 AFIO_EVCR;
	u32 AFIO_MAPR;
	u32 AFIO_EXTICR1;
	u32 AFIO_EXTICR2;
	u32 AFIO_EXTICR3;
	u32 AFIO_EXTICR4;
}AFIO;

/* For alternate function registers */
#define AFIO_REGS     ((AFIO*)0x40010000)

typedef struct
{
	u32 USART_SR;
	u32 USART_DR;
	u32 USART_BRR;
	u32 USART_CR1;
	u32 USART_CR2;
	u32 USART_CR3;
	u32 USART_GTPR;
}USART_REG;

/* Array of pointer to structure holding the base address of each USART channel */
static USART_REG *const UsartBaseAddresses[UART_NO_OF_CHANNEL] =
{
		USART1_BASE_ADDRESS,
		USART2_BASE_ADDRESS,
		USART3_BASE_ADDRESS,
};

/* Defining USART_SR bits */
#define CTS                 9
#define LBD                 8
#define TXE                 7
#define TC                  6
#define RXNE                5
#define IDLE                4
#define ORE                 3
#define NE                  2
#define FE                  1
#define PE                  0

/* Defining USART_CR1 bits */
#define UE                  13
#define M					12
#define WAKE				11
#define PCE					10
#define PS					9
#define PEIE				8
#define TXEIE				7
#define TCIE				6
#define RXNEIE				5
#define IDLEIE				4
#define TE					3
#define RE					2
#define RWU					1
#define SBK					0

/* Defining USART_CR2 bits */
#define LINEN      		 	14
#define STOP1				13
#define STOP0				12
#define CLKEN				11
#define CPOL				10
#define CPHA				9
#define LBCL				8
#define LBDIE				6
#define LBDL				5

/* Defining USART_CR3 bits */
#define CTSIE				10
#define CTSE				9
#define RTSE				8
#define DMAT				7
#define DMAR				6
#define SCEN				5
#define NACK				4
#define HDSEL				3
#define IRLP				2
#define IREN				1
#define EIE					0

#endif /* UART_PRIV_H */ 
