/*****************************************************************/
/* Author: Mahmoud Alaa Elwelily                                 */
/* Date: 27-Apr-19                                               */
/* Version: 01                                                   */
/* Description: Interface file for UART Driver for STM32F103x    */
/*****************************************************************/

/*Preprocessor Guard*/
#ifndef UART_INTERFACE_H 
#define UART_INTERFACE_H 

/*****************************************************************/
/*                   Driver Initialization                       */
/*****************************************************************/
/*
 * Description: Function to initialize the UART peripheral for the 3 UART channels
 * Inputs: none
 * Output: void
 */
void UART_vInit(void);

/*****************************************************************/
/*                   UART1 special functions                     */
/*****************************************************************/
/*
 * Description: Function to change UART1 pin mapping to PB6/PB7 instead of the default pin mapping
 * Inputs: none
 * Output: void
 */
void UART1_vChangeRemapping (void);

/*
 * Description: Function to change UART1 pin mapping the default pins PA8/PA9
 * Inputs: none
 * Output: void
 */
void UART1_vUnchangeRemapping (void);
/*****************************************************************/
/*                  UART Transmission functions                  */
/*****************************************************************/
/*
 * Description: Function to send a single character by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
u8 UART1_u8SendChar (u8 Copy_u8Char);

/*
 * Description: Function to send a single character by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
u8 UART2_u8SendChar (u8 Copy_u8Char);

/*
 * Description: Function to send a single character by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
u8 UART3_u8SendChar (u8 Copy_u8Char);

/*
 * Description: Synchronous Function to send buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART1_u8SendBufferSynch (const u8* Copy_pu8Buffer,u8 Copy_u8BufferSize);

/*
 * Description: Synchronous Function to send buffer by UART1 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART2_u8SendBufferSynch (const u8* Copy_pu8Buffer,u8 Copy_u8BufferSize);

/*
 * Description: Synchronous Function to send buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART3_u8SendBufferSynch (const u8* Copy_pu8Buffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start sending a buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * 	then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART1_u8SendBufferAsynch ( u8* Copy_pu8Buffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start sending a buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART2_u8SendBufferAsynch ( u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start sending a buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
u8 UART3_u8SendBufferAsynch ( u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART1
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART1_u8SetTxCallBack (void (*CallbackTxFunc)(void));

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART2
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART2_u8SetTxCallBack (void (*CallbackTxFunc)(void));

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART3
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART3_u8SetTxCallBack (void (*CallbackTxFunc)(void));

/*****************************************************************/
/*                    UART Receiving functions                   */
/*****************************************************************/
/*
 * Description:  Function to receive a single character by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
u8 UART1_u8ReceiveChar (u8 *Copy_u8Char);

/*
 * Description:  Function to receive a single character by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
u8 UART2_u8ReceiveChar (u8 *Copy_u8Char);

/*
 * Description:  Function to receive a single character by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
u8 UART3_u8ReceiveChar (u8 *Copy_u8Char);

/*
 * Description: Synchronous Function to receive buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
u8 UART1_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Synchronous Function to receive buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
u8 UART2_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Synchronous Function to receive buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
u8 UART3_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start receiving buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
u8 UART1_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start receiving buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
u8 UART2_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Asynchronous Function to start receiving buffer by UART1 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
u8 UART3_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize);

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer receiving for UART1
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART1_u8SetRxCallBack (void (*CallbackRxFunc)(void));

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer receiving for UART2
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART2_u8SetRxCallBack (void (*CallbackRxFunc)(void));

/*
 * Description: Function to set the RX callback function required to be called after finishing the buffer receiving for UART3
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
u8 UART3_u8SetRxCallBack (void (*CallbackRxFunc)(void));

#endif /* UART_INTERFACE_H */ 
