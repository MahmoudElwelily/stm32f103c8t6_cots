/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 1st, 2019                 	*/
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#include "STD_TYPES.h"
#include "BIT_CALC.h"

#include "NVIC_interface.h"
#include "NVIC_config.h"
#include "NVIC_private.h"

#define STD_u8_ERROR 			(u8)1
#define STD_u8_OK				(u8)0
static void voidCalculateInterruptRegAndBit(u8 Copy_u8InterruptNum, NVIC_u32ptr *Copy_Pu32BaseAddressPtr)
{
	u8 Local_u8InterruptBitNum;
	u8 Local_u8InterruptRegNum;

	Local_u8InterruptRegNum = Copy_u8InterruptNum / 32;
	Local_u8InterruptBitNum = Copy_u8InterruptNum % 32;
	SET_BIT(Copy_Pu32BaseAddressPtr->Regs[Local_u8InterruptRegNum], Local_u8InterruptBitNum);
}


u8 NVIC_u8EnableInterrupt(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8InterruptNum < NVIC_INTERRUPTS_NUMBER)
	{
		voidCalculateInterruptRegAndBit(Copy_u8InterruptNum, ISER_ptr);
		Local_u8Error = STD_u8_OK;
	}


	return Local_u8Error;
}
u8 NVIC_u8DisableInterrupt(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8InterruptNum < NVIC_INTERRUPTS_NUMBER)
	{
		voidCalculateInterruptRegAndBit(Copy_u8InterruptNum, ICER_ptr);
		Local_u8Error = STD_u8_OK;
	}


	return Local_u8Error;
}
u8 NVIC_u8SetPendingFlag(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8InterruptNum < NVIC_INTERRUPTS_NUMBER)
	{
		voidCalculateInterruptRegAndBit(Copy_u8InterruptNum, ISPR_ptr);
		Local_u8Error = STD_u8_OK;
	}


	return Local_u8Error;
}

u8 NVIC_u8ClearPendingFlag(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8InterruptNum < NVIC_INTERRUPTS_NUMBER)
	{
		voidCalculateInterruptRegAndBit(Copy_u8InterruptNum, ICPR_ptr);
		Local_u8Error = STD_u8_OK;
	}


	return Local_u8Error;
}
u8 NVIC_u8SetPriority(u8 Copy_u8InterruptNum, u8 Copy_u8GroupPriority, u8 Copy_u8SubPriority)
{
	u8 Local_u8Error = STD_u8_ERROR;


	if((Copy_u8InterruptNum < NVIC_INTERRUPTS_NUMBER) &&
	   (Copy_u8GroupPriority < 16) &&
	   (Copy_u8SubPriority < 16))
	{

		IPR_ptr->Regs[Copy_u8InterruptNum] = ((Copy_u8GroupPriority << 6) | (Copy_u8SubPriority << 4));

		Local_u8Error = STD_u8_OK;
	}


	return Local_u8Error;
}

