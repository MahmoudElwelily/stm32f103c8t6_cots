/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 1st, 2019                 	*/
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#ifndef NVIC_PRIVATE_H_
#define NVIC_PRIVATE_H_



#define SCB_AIRCR				((Register_u32*) ((u32)(0xE000E008) + (u32)(0x0C)) )


#define NVIC_INTERRUPTS_NUMBER	(u8)60


typedef struct
{
	u32 Regs[3];
}NVIC_u32ptr;

typedef struct
{
	u8 Regs[NVIC_INTERRUPTS_NUMBER];
}NVIC_u8ptr;




NVIC_u32ptr *ISER_ptr=  (NVIC_u32ptr*) (0xE000E100);
NVIC_u32ptr *ICER_ptr=  (NVIC_u32ptr*) (0xE000E180);
NVIC_u32ptr *ISPR_ptr=  (NVIC_u32ptr*) (0xE000E200);
NVIC_u32ptr *ICPR_ptr=  (NVIC_u32ptr*) (0xE000E280);
NVIC_u32ptr *IABR_ptr=  (NVIC_u32ptr*) (0xE000E300);

NVIC_u8ptr *IPR_ptr  =  (NVIC_u8ptr*)  (0xE000E400);


static void voidCalculateInterruptRegAndBit(u8 Copy_u8InterruptNum, NVIC_u32ptr *Copy_Pu32BaseAddressPtr);





#endif /* NVIC_PRIVATE_H_ */
