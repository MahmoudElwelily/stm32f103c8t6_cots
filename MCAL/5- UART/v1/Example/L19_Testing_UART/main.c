#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "DELAY_interface.h"
#include "RCC_interface.h"
#include "DIO_interface.h"
#include "UART_interface.h"
#include "NVIC_interface.h"

u8 name[]= "mahmoud";

//void TX_ISR (void);
void main (void)
{
	RCC_vInit ();
	RCC_vEnablePeripheralClock (RCC_u8_IOPB);
	RCC_vEnablePeripheralClock (RCC_u8_AFIO);
	RCC_vEnablePeripheralClock (RCC_u8_USART1);
	UART1_vChangeRemapping();
	DIO_vInit ();
	//NVIC_u8EnableInterrupt(39);
	UART_vInit();
	//UART3_u8SetTxCallBack (TX_ISR);

	while(1)
	{
		UART1_u8SendBufferSynch (name,7);
  	//UART3_u8SendBufferAsynch (name,7);
		delay_milliseconds(1000);
	}
}

/*
void TX_ISR (void)
{
	static u8 value = 0;
	if (value == 0)
	{
		DIO_u8SetPinValue (DIO_u8_PIN0,DIO_u8_PIN_HIGH);
		value = 1;
	}
	else
	{
		DIO_u8SetPinValue (DIO_u8_PIN0,DIO_u8_PIN_LOW);
		value = 0;
	}
}*/

