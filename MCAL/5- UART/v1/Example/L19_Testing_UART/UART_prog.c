/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 27-Apr-19                                            */
/* Version: 01                                                */
/* Description: Program file for UART Driver                  */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "UART_interface.h" 
#include "UART_priv.h" 
#include "UART_config.h" 

#if (UART1_STATE == UART_ENABLE)
/*Macro to specify the USARTDIV1 that will assigned to the USART_BRR register*/
#define USARTDIV1_VALUE      ((FCPU) / (UART1_u32_BAUDRATE * 16UL))

/*Static pointer to the passed string needed to be sent using the Asynchronous transmitting function for UART1*/
static u8 *UART1_pu8TransmittedString;

/*Static pointer to the passed array needed to be filled using the Asynchronous receiving function for UART1*/
static u8 *UART1_pu8ReceivedString;

/*Static variable that will be equal the required length of the transmitted msg for UART1*/
static u8 UART1_u8TransmittedMsgSize;

/*Static variable that will be equal the required length of the received msg for UART1*/
static u8 UART1_u8ReceivedMsgSize;

/* Incremented indices used in asynchronous transmitting and receiving for UART1 */
static u8 UART1_u8TransIndex;
static u8 UART1_u8RecIndex;

/* Sending and receiving flags, used in Asynchronous transmitting and receiving */
static u8 UART1_u8IsSendingFlag = 0;
static u8 UART1_u8IsReceivingFlag = 0;

/* Pointer to call back function for UART1 asynchronous transmitting and receiving */
static void (*UART1_TXPtrAsychCallback) (void) = NULL;
static void (*UART1_RXPtrAsychCallback) (void) = NULL;
#endif

#if (UART2_STATE == UART_ENABLE)
/*Macro to specify the USARTDIV2 that will assigned to the USART_BRR register*/
#define USARTDIV2_VALUE      ((FCPU) / (UART2_u32_BAUDRATE * 16UL))

/*Static pointer to the passed string needed to be sent using the Asynchronous transmitting function for UART2*/
static u8 *UART2_pu8TransmittedString;

/*Static pointer to the passed array needed to be filled using the Asynchronous receiving function for UART2*/
static u8 *UART2_pu8ReceivedString;

/*Static variable that will be equal the required length of the transmitted msg for UART2*/
static u8 UART2_u8TransmittedMsgSize;
/*Static variable that will be equal the required length of the received msg for UART2*/
static u8 UART2_u8ReceivedMsgSize;

/* Incremented indices used in asynchronous transmitting and receiving for UART2 */
static u8 UART2_u8TransIndex;
static u8 UART2_u8RecIndex;

/* Sending and receiving flags, used in Asynchronous transmitting and receiving */
static u8 UART2_u8IsSendingFlag = 0;
static u8 UART2_u8IsReceivingFlag = 0;

/* Pointer to call back function for UART2 asynchronous transmitting and receiving */
static void (*UART2_TXPtrAsychCallback) (void) = NULL;
static void (*UART2_RXPtrAsychCallback) (void) = NULL;
#endif

#if (UART3_STATE == UART_ENABLE)

/*Macro to specify the USARTDIV3 that will assigned to the USART_BRR register*/
#define USARTDIV3_VALUE      ((FCPU) / (UART3_u32_BAUDRATE * 16UL))

/*Static pointer to the passed string needed to be sent using the Asynchronous transmitting function for UART3*/
static u8 *UART3_pu8TransmittedString;

/*Static pointer to the passed array needed to be filled using the Asynchronous receiving function for UART3*/
static u8 *UART3_pu8ReceivedString;

/*Static variable that will be equal the required length of the transmitted msg for UART3*/
static u8 UART3_u8TransmittedMsgSize;
/*Static variable that will be equal the required length of the received msg for UART3*/
static u8 UART3_u8ReceivedMsgSize;

/* Incremented indices used in asynchronous transmitting and receiving for UART3 */
static u8 UART3_u8TransIndex;
static u8 UART3_u8RecIndex;

/* Sending and receiving flags, used in Asynchronous transmitting and receiving */
static u8 UART3_u8IsSendingFlag = 0;
static u8 UART3_u8IsReceivingFlag = 0;

/* Pointer to call back function for UART3 asynchronous transmitting and receiving */
static void (*UART3_TXPtrAsychCallback) (void) = NULL;
static void (*UART3_RXPtrAsychCallback) (void) = NULL;
#endif

/*****************************************************************/
/*                   Driver Initialization                       */
/*****************************************************************/
/*
 * Description: Function to initialize the UART peripheral for the 3 UART channels
 * Inputs: none
 * Output: void
 */
void UART_vInit(void)
{
	f32 Local_f32BaudRateMan1;
	f32 Local_f32BaudRateFrac1;
	f32 Local_f32BaudRateMan2;
	f32 Local_f32BaudRateFrac2;
	f32 Local_f32BaudRateMan3;
	f32 Local_f32BaudRateFrac3;

	#if (UART1_STATE == UART_ENABLE)
	    /* Checking the state of UART,Transmitter and receiver */
		UsartBaseAddresses[UART1]->USART_CR1 |= (1<<UE);
		if (UART1_TX_STATE == UART_ENABLE)
			UsartBaseAddresses[UART1]->USART_CR1 |= (1<<TE);
		if (UART1_RX_STATE == UART_ENABLE)
			UsartBaseAddresses[UART1]->USART_CR1 |= (1<<RE);

		/* Checking the configured baud rate */
		Local_f32BaudRateMan1 =  ((FCPU) / (UART1_u32_BAUDRATE * 16UL));
		Local_f32BaudRateFrac1 = (16*(Local_f32BaudRateMan1-(u32)Local_f32BaudRateMan1));
		UsartBaseAddresses[UART1]->USART_BRR |= ((u32)Local_f32BaudRateMan1 << 4);
		UsartBaseAddresses[UART1]->USART_BRR |= ((u32)Local_f32BaudRateFrac1 << 0);

		/* Checking the states of interrupts (TXEI,RXNEI,TCI) */
		if (UART1_TXEI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART1]->USART_CR1 |= (1<<TXEIE);
		if (UART1_RXNEI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART1]->USART_CR1 |= (1<<RXNEIE);
		if (UART1_TCI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART1]->USART_CR1 |= (1<<TCIE);

		/* Checking the parity state */
		switch (UART1_u8_PARITY)
		{
			case UART_u8_NO_PARITY:
				UsartBaseAddresses[UART1]->USART_CR1 &= ~(1<<PCE);
				break;

			case UART_u8_EVEN_PARITY:
				UsartBaseAddresses[UART1]->USART_CR1 |= (1<<PCE);
				UsartBaseAddresses[UART1]->USART_CR1 &= ~(1<<PS);
				break;

			case UART_u8_ODD_PARITY:
				UsartBaseAddresses[UART1]->USART_CR1 |= (1<<PCE);
				UsartBaseAddresses[UART1]->USART_CR1 |= (1<<PS);
				break;
		}

		/* Checking the number of stop bit */
		switch (UART1_u8_NO_OF_STP_BITS)
		{
			case UART_u8_HALF_BIT:
				UsartBaseAddresses[UART1]->USART_CR2 |= (1<<12);
				break;

			case UART_u8_ONE_BIT:
				UsartBaseAddresses[UART1]->USART_CR2 |= (0<<12);
				break;

			case UART_u8_ONE_AND_HALF_BIT:
				UsartBaseAddresses[UART1]->USART_CR2 |= (3<<12);
				break;

			case UART_u8_TWO_BITS:
				UsartBaseAddresses[UART1]->USART_CR2 |= (2<<12);
				break;
		}

		/* Checking the data length */
		switch (UART1_u8_DATA_SIZE)
		{
			case UART_u8_EIGHT_BITS:
				UsartBaseAddresses[UART1]->USART_CR1 &= ~(1<<M);
				break;

			case UART_u8_NINE_BITS:
				UsartBaseAddresses[UART1]->USART_CR1 |= (1<<M);
				break;
		}
	#endif

	#if (UART2_STATE == UART_ENABLE)
	    /* Checking the state of UART,Transmitter and receiver */
		UsartBaseAddresses[UART2]->USART_CR1 |= (1<<UE);
		if (UART2_TX_STATE == UART_ENABLE)
			UsartBaseAddresses[UART2]->USART_CR1 |= (1<<TE);
		if (UART2_RX_STATE == UART_ENABLE)
			UsartBaseAddresses[UART2]->USART_CR1 |= (1<<RE);

		/* Checking the configured baud rate */
		Local_f32BaudRateMan2 =  ((FCPU) / (UART2_u32_BAUDRATE * 16UL));
		Local_f32BaudRateFrac2 = (16*(Local_f32BaudRateMan2-(u32)Local_f32BaudRateMan2));
		UsartBaseAddresses[UART2]->USART_BRR |= ((u32)Local_f32BaudRateMan2 << 4);
		UsartBaseAddresses[UART2]->USART_BRR |= ((u32)Local_f32BaudRateFrac2 << 0);

		/* Checking the states of interrupts (TXEI,RXNEI,TCI) */
		if (UART2_TXEI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART2]->USART_CR1 |= (1<<TXEIE);
		if (UART2_RXNEI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART2]->USART_CR1 |= (1<<RXNEIE);
		if (UART2_TCI_STATE == UART_ENABLE)
			UsartBaseAddresses[UART2]->USART_CR1 |= (1<<TCIE);

		/* Checking the parity state */
		switch (UART2_u8_PARITY)
		{
			case UART_u8_NO_PARITY:
				UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<PCE);
				break;

			case UART_u8_EVEN_PARITY:
				UsartBaseAddresses[UART2]->USART_CR1 |= (1<<PCE);
				UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<PS);
				break;

			case UART_u8_ODD_PARITY:
				UsartBaseAddresses[UART2]->USART_CR1 |= (1<<PCE);
				UsartBaseAddresses[UART2]->USART_CR1 |= (1<<PS);
				break;
		}

		/* Checking the number of stop bit */
		switch (UART2_u8_NO_OF_STP_BITS)
		{
			case UART_u8_HALF_BIT:
				UsartBaseAddresses[UART2]->USART_CR2 |= (1<<12);
				break;

			case UART_u8_ONE_BIT:
				UsartBaseAddresses[UART2]->USART_CR2 |= (0<<12);
				break;

			case UART_u8_ONE_AND_HALF_BIT:
				UsartBaseAddresses[UART2]->USART_CR2 |= (3<<12);
				break;

			case UART_u8_TWO_BITS:
				UsartBaseAddresses[UART2]->USART_CR2 |= (2<<12);
				break;
		}

		/* Checking the data length */
		switch (UART2_u8_DATA_SIZE)
		{
			case UART_u8_EIGHT_BITS:
				UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<M);
				break;

			case UART_u8_NINE_BITS:
				UsartBaseAddresses[UART2]->USART_CR1 |= (1<<M);
				break;
		}
	#endif

	#if (UART3_STATE == UART_ENABLE)
    /* Checking the state of UART,Transmitter and receiver */
	UsartBaseAddresses[UART3]->USART_CR1 |= (1<<UE);
	if (UART3_TX_STATE == UART_ENABLE)
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<TE);
	if (UART3_RX_STATE == UART_ENABLE)
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<RE);

	/* Checking the configured baud rate */
	Local_f32BaudRateMan3 =  ((FCPU) / (UART3_u32_BAUDRATE * 16UL));
	Local_f32BaudRateFrac3 = (16*(Local_f32BaudRateMan3-(u32)Local_f32BaudRateMan3));
	UsartBaseAddresses[UART3]->USART_BRR |= ((u32)Local_f32BaudRateMan3 << 4);
	UsartBaseAddresses[UART3]->USART_BRR |= ((u32)Local_f32BaudRateFrac3 << 0);

	/* Checking the states of interrupts (TXEI,RXNEI,TCI) */
	if (UART3_TXEI_STATE == UART_ENABLE)
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<TXEIE);
	if (UART3_RXNEI_STATE == UART_ENABLE)
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<RXNEIE);
	if (UART3_TCI_STATE == UART_ENABLE)
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<TCIE);

	/* Checking the parity state */
	switch (UART3_u8_PARITY)
	{
		case UART_u8_NO_PARITY:
			UsartBaseAddresses[UART3]->USART_CR1 &= ~(1<<PCE);
			break;

		case UART_u8_EVEN_PARITY:
			UsartBaseAddresses[UART3]->USART_CR1 |= (1<<PCE);
			UsartBaseAddresses[UART3]->USART_CR1 &= ~(1<<PS);
			break;

		case UART_u8_ODD_PARITY:
			UsartBaseAddresses[UART3]->USART_CR1 |= (1<<PCE);
			UsartBaseAddresses[UART3]->USART_CR1 |= (1<<PS);
			break;
	}

	/* Checking the number of stop bit */
	switch (UART3_u8_NO_OF_STP_BITS)
	{
		case UART_u8_HALF_BIT:
			UsartBaseAddresses[UART3]->USART_CR2 |= (1<<12);
			break;

		case UART_u8_ONE_BIT:
			UsartBaseAddresses[UART3]->USART_CR2 |= (0<<12);
			break;

		case UART_u8_ONE_AND_HALF_BIT:
			UsartBaseAddresses[UART3]->USART_CR2 |= (3<<12);
			break;

		case UART_u8_TWO_BITS:
			UsartBaseAddresses[UART3]->USART_CR2 |= (2<<12);
			break;
	}

	/* Checking the data length */
	switch (UART3_u8_DATA_SIZE)
	{
		case UART_u8_EIGHT_BITS:
			UsartBaseAddresses[UART3]->USART_CR1 &= ~(1<<M);
			break;

		case UART_u8_NINE_BITS:
			UsartBaseAddresses[UART3]->USART_CR1 |= (1<<M);
			break;
	}
	#endif

	return;
}

/*****************************************************************/
/*                   UART1 special functions                     */
/*****************************************************************/
/*
 * Description: Function to change UART1 pin mapping to PB6/PB7 instead of the default pin mapping
 * Inputs: none
 * Output: void
 */
#if (UART1_STATE == UART_ENABLE)
	void UART1_vChangeRemapping (void)
	{
		AFIO_REGS->AFIO_MAPR |= (1<<2);
	}
#endif
/*
 * Description: Function to change UART1 pin mapping the default pins PA8/PA9
 * Inputs: none
 * Output: void
 */
#if (UART1_STATE == UART_ENABLE)
	void UART1_vUnchangeRemapping (void)
	{
		AFIO_REGS->AFIO_MAPR &= ~(1<<2);
	}
#endif
/*****************************************************************/
/*                  UART Transmission functions                  */
/*****************************************************************/
/*
 * Description: Function to send a single character by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8SendChar (u8 Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	/* Put data into buffer, sends the data */
	UsartBaseAddresses[UART1]->USART_DR = Copy_u8Char;

	/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
	 * the fault timeout */
	while (GET_BIT(UsartBaseAddresses[UART1]->USART_SR,TXE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
	{
		Local_u16Timeout++;
	}

	if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
		Local_u8Error = ERROR_NOK;

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Function to send a single character by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8SendChar (u8 Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	/* Put data into buffer, sends the data */
	UsartBaseAddresses[UART2]->USART_DR = Copy_u8Char;

	/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
	 * the fault timeout */
	while (GET_BIT(UsartBaseAddresses[UART2]->USART_SR,TXE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
	{
		Local_u16Timeout++;
	}

	if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
		Local_u8Error = ERROR_NOK;

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Function to send a single character by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: the character needed to be sent
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8SendChar (u8 Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	/* Put data into buffer, sends the data */
	UsartBaseAddresses[UART3]->USART_DR = Copy_u8Char;

	/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
	 * the fault timeout */
	while (GET_BIT(UsartBaseAddresses[UART3]->USART_SR,TXE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
	{
		Local_u16Timeout++;
	}

	if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
		Local_u8Error = ERROR_NOK;

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to send buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8SendBufferSynch (const u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_pu8TransmittedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize ; Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;

			/* Put data into buffer, sends the data */
			UsartBaseAddresses[UART1]->USART_DR = Copy_pu8TransmittedBuffer[Local_u8LoopIndex];

			/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART1]->USART_SR,TXE)  == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to send buffer by UART1 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8SendBufferSynch (const u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_pu8TransmittedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize ; Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;

			/* Put data into buffer, sends the data */
			UsartBaseAddresses[UART2]->USART_DR = Copy_pu8TransmittedBuffer[Local_u8LoopIndex];

			/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART2]->USART_SR,TXE)  == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to send buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8SendBufferSynch (const u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_pu8TransmittedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize ; Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;

			/* Put data into buffer, sends the data */
			UsartBaseAddresses[UART3]->USART_DR = Copy_pu8TransmittedBuffer[Local_u8LoopIndex];

			/* Wait for empty transmit buffer and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART3]->USART_SR,TXE)  == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout == UART_u16_FAULT_TIMEOUT)
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Asynchronous Function to start sending a buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * 	then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8SendBufferAsynch ( u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (Copy_pu8TransmittedBuffer != NULL && UART1_u8IsSendingFlag == 0)
	{
		UART1_u8IsSendingFlag = 1;
		UART1_u8TransIndex = 0;
		UART1_u8TransmittedMsgSize = Copy_u8BufferSize;
		UART1_pu8TransmittedString = Copy_pu8TransmittedBuffer;
		/*Enabling Data Register Empty interrupt enable the tx interrupt in the init,try it first */
		//SET_BIT(UART_UCSRB,UDRIE);
		UsartBaseAddresses[UART1]->USART_DR = Copy_pu8TransmittedBuffer[UART1_u8TransIndex];
		UART1_u8TransIndex++;		/*Enabling Transmit Data Register Empty interrupt enable the tx interrupt in the init */
		UsartBaseAddresses[UART1]->USART_CR1 |= (1<<TXEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Asynchronous Function to start sending a buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8SendBufferAsynch ( u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (Copy_pu8TransmittedBuffer != NULL && UART2_u8IsSendingFlag == 0)
	{
		UART2_u8IsSendingFlag = 1;
		UART2_u8TransIndex = 0;
		UART2_u8TransmittedMsgSize = Copy_u8BufferSize;
		UART2_pu8TransmittedString = Copy_pu8TransmittedBuffer;
		/*Enabling Data Register Empty interrupt enable the tx interrupt in the init,try it first */
		//SET_BIT(UART_UCSRB,UDRIE);
		UsartBaseAddresses[UART2]->USART_DR = UART2_pu8TransmittedString[UART2_u8TransIndex];
		UART2_u8TransIndex++;
		/*Enabling Transmit Data Register Empty interrupt enable the tx interrupt in the init */
		UsartBaseAddresses[UART2]->USART_CR1 |= (1<<TXEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif


/*
 * Description: Asynchronous Function to start sending a buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * then the Data Register Empty interrupt will send the rest of the string continuously
 * Inputs: pointer to the buffer and the buffer size
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8SendBufferAsynch ( u8* Copy_pu8TransmittedBuffer,u8 Copy_u8BufferSize)
{

	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (Copy_pu8TransmittedBuffer != NULL && UART3_u8IsSendingFlag == 0)
	{
		UART3_u8IsSendingFlag = 1;
		UART3_u8TransIndex = 0;
		UART3_u8TransmittedMsgSize = Copy_u8BufferSize;
		UART3_pu8TransmittedString = Copy_pu8TransmittedBuffer;
		UsartBaseAddresses[UART3]->USART_DR = UART3_pu8TransmittedString[UART3_u8TransIndex];
		UART3_u8TransIndex++;
		/*Enabling Transmit Data Register Empty interrupt enable the tx interrupt in the init */
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<TXEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART1
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8SetTxCallBack (void (*CallbackTxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackTxFunc != NULL)
		UART1_TXPtrAsychCallback = CallbackTxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART2
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8SetTxCallBack (void (*CallbackTxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackTxFunc != NULL)
		UART2_TXPtrAsychCallback = CallbackTxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer transmission for UART3
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8SetTxCallBack (void (*CallbackTxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackTxFunc != NULL)
		UART3_TXPtrAsychCallback = CallbackTxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/*****************************************************************/
/*                    UART Receiving functions                   */
/*****************************************************************/
/*
 * Description:  Function to receive a single character by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8ReceiveChar (u8 *Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	if (Copy_u8Char != NULL)
	{
		/* Wait for empty transmit buffer and checking that the timeout var doesn't exceed
		 * the fault timeout */
		while (GET_BIT(UsartBaseAddresses[UART1]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
		{
			Local_u16Timeout++;
		}

		if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
			/* Get and return received data from buffer */
			*Copy_u8Char = UsartBaseAddresses[UART1]->USART_DR;
		else
			Local_u8Error = ERROR_NOK;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description:  Function to receive a single character by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8ReceiveChar (u8 *Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	if (Copy_u8Char != NULL)
	{
		/* Wait for empty transmit buffer and checking that the timeout var doesn't exceed
		 * the fault timeout */
		while (GET_BIT(UsartBaseAddresses[UART2]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
		{
			Local_u16Timeout++;
		}

		if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
			/* Get and return received data from buffer */
			*Copy_u8Char = UsartBaseAddresses[UART2]->USART_DR;
		else
			Local_u8Error = ERROR_NOK;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description:  Function to receive a single character by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs:  pointer to the variable that will hold the received character
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8ReceiveChar (u8 *Copy_u8Char)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Timeout = 0;

	if (Copy_u8Char != NULL)
	{
		/* Wait for empty transmit buffer and checking that the timeout var doesn't exceed
		 * the fault timeout */
		while (GET_BIT(UsartBaseAddresses[UART3]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
		{
			Local_u16Timeout++;
		}

		if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
			/* Get and return received data from buffer */
			*Copy_u8Char = UsartBaseAddresses[UART3]->USART_DR;
		else
			Local_u8Error = ERROR_NOK;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to receive buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_u8ReceivedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize;Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;
			/* Wait for data to be received and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART1]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
				/* Get and return received data from buffer */
				Copy_u8ReceivedBuffer[Local_u8LoopIndex] = UsartBaseAddresses[UART1]->USART_DR;
			else
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to receive buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_u8ReceivedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize;Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;
			/* Wait for data to be received and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART2]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
				/* Get and return received data from buffer */
				Copy_u8ReceivedBuffer[Local_u8LoopIndex] = UsartBaseAddresses[UART2]->USART_DR;
			else
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Synchronous Function to receive buffer by UART3 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * Inputs: a pointer to the buffer which will hold the received bytes and the buffer Size
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8ReceiveBufferSynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8LoopIndex;
	u16 Local_u16Timeout;

	if (Copy_u8ReceivedBuffer != NULL)
	{
		for (Local_u8LoopIndex = 0 ; Local_u8LoopIndex < Copy_u8BufferSize;Local_u8LoopIndex++)
		{
			Local_u16Timeout = 0;
			/* Wait for data to be received and checking that the timeout variable doesn't exceed
			 * the fault timeout */
			while (GET_BIT(UsartBaseAddresses[UART3]->USART_SR,RXNE) == 0 && Local_u16Timeout < UART_u16_FAULT_TIMEOUT)
			{
				Local_u16Timeout++;
			}

			if (Local_u16Timeout != UART_u16_FAULT_TIMEOUT)
				/* Get and return received data from buffer */
				Copy_u8ReceivedBuffer[Local_u8LoopIndex] = UsartBaseAddresses[UART3]->USART_DR;
			else
			{
				Local_u8Error = ERROR_NOK;
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Asynchronous Function to start receiving buffer by UART1 knowing that:
 * UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;


	if (Copy_u8ReceivedBuffer != NULL && Copy_u8BufferSize > 0 && UART1_u8IsReceivingFlag == 0)
	{
		UART1_u8IsReceivingFlag = 1;
		UART1_u8RecIndex = 0;
		UART1_pu8ReceivedString = Copy_u8ReceivedBuffer;
		UART1_u8ReceivedMsgSize = Copy_u8BufferSize;
		/*Enabling Read data register not empty interrupt */
		UsartBaseAddresses[UART1]->USART_CR1 |= (1<<RXNEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Asynchronous Function to start receiving buffer by UART2 knowing that:
 * UART2 corresponds to pins PA2(TX2),PA3(RX2)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;


	if (Copy_u8ReceivedBuffer != NULL && Copy_u8BufferSize > 0 && UART2_u8IsReceivingFlag == 0)
	{
		UART2_u8IsReceivingFlag = 1;
		UART2_u8RecIndex = 0;
		UART2_pu8ReceivedString = Copy_u8ReceivedBuffer;
		UART2_u8ReceivedMsgSize = Copy_u8BufferSize;
		/*Enabling Read data register not empty interrupt */
		UsartBaseAddresses[UART2]->USART_CR1 |= (1<<RXNEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Asynchronous Function to start receiving buffer by UART1 knowing that:
 * UART3 corresponds to pins PB10(TX3),PB11(RX3)
 * then the Rx Complete interrupt will receive the rest of the buffer continuously
 * Inputs: a pointer to the buffer which will hold the received bytes + the buffer Size
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8ReceiveStringAsynch (u8* Copy_u8ReceivedBuffer,u8 Copy_u8BufferSize)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;


	if (Copy_u8ReceivedBuffer != NULL && Copy_u8BufferSize > 0 && UART3_u8IsReceivingFlag == 0)
	{
		UART3_u8IsReceivingFlag = 1;
		UART3_u8RecIndex = 0;
		UART3_pu8ReceivedString = Copy_u8ReceivedBuffer;
		UART3_u8ReceivedMsgSize = Copy_u8BufferSize;
		/*Enabling Read data register not empty interrupt */
		UsartBaseAddresses[UART3]->USART_CR1 |= (1<<RXNEIE);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}

	/*Function return*/
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer receiving for UART1
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART1_STATE == UART_ENABLE)
u8 UART1_u8SetRxCallBack (void (*CallbackRxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackRxFunc != NULL)
		UART1_RXPtrAsychCallback = CallbackRxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the TX callback function required to be called after finishing the buffer receiving for UART2
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART2_STATE == UART_ENABLE)
u8 UART2_u8SetRxCallBack (void (*CallbackRxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackRxFunc != NULL)
		UART2_RXPtrAsychCallback = CallbackRxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/*
 * Description: Function to set the RX callback function required to be called after finishing the buffer receiving for UART3
 * Inputs: pointer to the callback function
 * Output: the Error state of the function
 */
#if (UART3_STATE == UART_ENABLE)
u8 UART3_u8SetRxCallBack (void (*CallbackRxFunc)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (CallbackRxFunc != NULL)
		UART3_RXPtrAsychCallback = CallbackRxFunc;
	else
		Local_u8Error = ERROR_NOK;
	return Local_u8Error;
}
#endif

/************************************************************/
/*      ISRs of UART interrupts                             */
/************************************************************/
/* The ISR of USART1 */
#if (UART1_STATE == UART_ENABLE)
void USART1_IRQHandler(void)
{
	if ((GET_BIT(UsartBaseAddresses[UART1]->USART_SR,TXE) == 1) && (UART1_u8IsSendingFlag == 1))
	{
		if (UART1_u8TransIndex == UART1_u8TransmittedMsgSize)
		{
			UART1_u8IsSendingFlag = 0;
			UART1_u8TransIndex = 0;
			UsartBaseAddresses[UART1]->USART_CR1 &= ~(1<<TXEIE);
			UART1_TXPtrAsychCallback();
		}
		else
		{
			UsartBaseAddresses[UART1]->USART_DR = UART1_pu8TransmittedString[UART1_u8TransIndex];
			UART1_u8TransIndex++;
		}
	}
	else if ((GET_BIT(UsartBaseAddresses[UART1]->USART_SR,RXNE) == 1) && (UART1_u8IsReceivingFlag== 1))
	{
		UsartBaseAddresses[UART1]->USART_SR &= ~(1<<RXNE);
		UART1_pu8ReceivedString[UART1_u8RecIndex] = UsartBaseAddresses[UART1]->USART_DR;
		UART1_u8RecIndex++;
		if (UART1_u8RecIndex == UART1_u8ReceivedMsgSize)
		{
			UART1_u8IsReceivingFlag = 0;
			UART1_u8RecIndex = 0;
			UsartBaseAddresses[UART1]->USART_CR1 &= ~(1<<RXNEIE);
			UART1_RXPtrAsychCallback();
		}
	}
}
#endif

/* The ISR of USART2 */
#if (UART2_STATE == UART_ENABLE)
void USART2_IRQHandler(void)
{
	if ((GET_BIT(UsartBaseAddresses[UART2]->USART_SR,TXE) == 1) && (UART2_u8IsSendingFlag == 1))
	{
		if (UART2_u8TransIndex == UART2_u8TransmittedMsgSize)
		{
			UART2_u8IsSendingFlag = 0;
			UART2_u8TransIndex = 0;
			UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<TXEIE);
			UART2_TXPtrAsychCallback();
		}
		else
		{
			UsartBaseAddresses[UART2]->USART_DR = UART2_pu8TransmittedString[UART2_u8TransIndex];
			UART2_u8TransIndex++;
		}
	}
	else if ((GET_BIT(UsartBaseAddresses[UART2]->USART_SR,RXNE) == 1) && (UART2_u8IsReceivingFlag== 1))
	{
		UsartBaseAddresses[UART2]->USART_SR &= ~(1<<RXNE);
		UART2_pu8ReceivedString[UART2_u8RecIndex] = UsartBaseAddresses[UART2]->USART_DR;
		UART2_u8RecIndex++;
		if (UART2_u8RecIndex == UART2_u8ReceivedMsgSize)
		{
			UART2_u8IsReceivingFlag = 0;
			UART2_u8RecIndex = 0;
			UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<RXNEIE);
			UART2_RXPtrAsychCallback();
		}
	}
}
#endif

/* The ISR of USART3 */
#if (UART3_STATE == UART_ENABLE)
void USART3_IRQHandler(void)
{
	if ((GET_BIT(UsartBaseAddresses[UART3]->USART_SR,TXE) == 1) && (UART3_u8IsSendingFlag == 1))
	{
		if (UART3_u8TransIndex == UART3_u8TransmittedMsgSize)
		{
			UART3_u8IsSendingFlag = 0;
			UART3_u8TransIndex = 0;
			UsartBaseAddresses[UART3]->USART_CR1 &= ~(1<<TXEIE);
			UART3_TXPtrAsychCallback();
		}
		else
		{
			UsartBaseAddresses[UART3]->USART_DR = UART3_pu8TransmittedString[UART3_u8TransIndex];
			UART3_u8TransIndex++;
		}
	}
	else if ((GET_BIT(UsartBaseAddresses[UART3]->USART_SR,RXNE) == 1) && (UART3_u8IsReceivingFlag== 1))
	{
		UsartBaseAddresses[UART3]->USART_SR &= ~(1<<RXNE);
		UART3_pu8ReceivedString[UART3_u8RecIndex] = UsartBaseAddresses[UART3]->USART_DR;
		UART3_u8RecIndex++;
		if (UART3_u8RecIndex == UART3_u8ReceivedMsgSize)
		{
			UART3_u8IsReceivingFlag = 0;
			UART3_u8RecIndex = 0;
			UsartBaseAddresses[UART2]->USART_CR1 &= ~(1<<RXNEIE);
			UART3_RXPtrAsychCallback();
		}
	}
}
#endif


