/************************************************************************/
/* Author: Mahmoud Alaa Elwelily                                        */
/* Date: 27-Apr-19                                                      */
/* Version: 01                                                          */
/* Description: Configuration file for UART Driver for STM32F103x       */
/************************************************************************/

/*Preprocessor Guard*/
#ifndef UART_CONFIG_H 
#define UART_CONFIG_H 


/*Macro to define the System clock which feeds the UART peripherl*/
#define FCPU               8000000

/*  Macro to specify the UART state of the 3 UART channels
 *  knowing that:
 *  UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 *  UART2 corresponds to pins PA2(TX2),PA3(RX2)
 *  UART3 corresponds to pins PB10(TX3),PB11(RX3)
 *  Ranges: UART_ENABLE
		    UART_DISABLE
*/
#define UART1_STATE		 UART_ENABLE
#define UART2_STATE      UART_DISABLE
#define UART3_STATE      UART_DISABLE

/*  Macro to specify the UART Transmitter state of the 3 UART channels
 *  knowing that:
 *  UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 *  UART2 corresponds to pins PA2(TX2),PA3(RX2)
 *  UART3 corresponds to pins PB10(TX3),PB11(RX3)
 *  Ranges: UART_ENABLE
		    UART_DISABLE
*/
#define UART1_TX_STATE	    UART_ENABLE
#define UART2_TX_STATE      UART_ENABLE
#define UART3_TX_STATE      UART_ENABLE

/*  Macro to specify the UART Receiver state of the 3 UART channels
 *  knowing that:
 *  UART1 corresponds to pins PA9(TX1),PA10(RX1) or pins PB6(TX1),PB7(RX1)
 *  UART2 corresponds to pins PA2(TX2),PA3(RX2)
 *  UART3 corresponds to pins PB10(TX3),PB11(RX3)
 *  Ranges: UART_ENABLE
		    UART_DISABLE
*/
#define UART1_RX_STATE	    UART_ENABLE
#define UART2_RX_STATE      UART_ENABLE
#define UART3_RX_STATE      UART_ENABLE

/*Macro to specify the desired UART Baudrate for each of th 3 channels*/
#define UART1_u32_BAUDRATE   (u32)9600
#define UART2_u32_BAUDRATE   (u32)9600
#define UART3_u32_BAUDRATE   (u32)9600

/*
  Macro to specify state of the TXE interrupt for each of the 3 channels
  Ranges: UART_ENABLE
		  UART_DISABLE
*/
#define UART1_TXEI_STATE         UART_DISABLE
#define UART2_TXEI_STATE         UART_DISABLE
#define UART3_TXEI_STATE         UART_DISABLE

/*
  Macro to specify state of the RXE interrupt for each of the 3 channels
  Ranges: UART_ENABLE
		  UART_DISABLE
*/
#define UART1_RXNEI_STATE         UART_DISABLE
#define UART2_RXNEI_STATE         UART_DISABLE
#define UART3_RXNEI_STATE         UART_DISABLE

/*
  Macro to specify state of the Transmission complete interrupt for each of the 3 channels
  Ranges: UART_ENABLE
		  UART_DISABLE
*/
#define UART1_TCI_STATE         UART_DISABLE
#define UART2_TCI_STATE         UART_DISABLE
#define UART3_TCI_STATE         UART_DISABLE
/*
  Macro to select the parity type for each of the 3 channels
  Ranges: UART_u8_NO_PARITY
		  UART_u8_EVEN_PARITY
		  UART_u8_ODD_PARITY
*/
#define UART1_u8_PARITY			UART_u8_NO_PARITY
#define UART2_u8_PARITY			UART_u8_NO_PARITY
#define UART3_u8_PARITY			UART_u8_NO_PARITY

/*
  Macro to select the number of the stop bits
  Ranges: UART_u8_HALF_BIT
  	  	  UART_u8_ONE_BIT
  	  	  UART_u8_ONE_AND_HALF_BIT
		  UART_u8_TWO_BITS
*/
#define UART1_u8_NO_OF_STP_BITS  UART_u8_ONE_BIT
#define UART2_u8_NO_OF_STP_BITS  UART_u8_ONE_BIT
#define UART3_u8_NO_OF_STP_BITS  UART_u8_ONE_BIT

/*
  Macro to select the number of the stop bits for each of the 3 channels
  Ranges:
		  UART_u8_EIGHT_BITS
		  UART_u8_NINE_BITS
*/
#define UART1_u8_DATA_SIZE		UART_u8_EIGHT_BITS
#define UART2_u8_DATA_SIZE		UART_u8_EIGHT_BITS
#define UART3_u8_DATA_SIZE		UART_u8_EIGHT_BITS

/* Macro to define the timeout which determines the fault time in the UART TX/RX */
#define UART_u16_FAULT_TIMEOUT    (u16)40000

#endif /* UART_CONFIG_H */ 

