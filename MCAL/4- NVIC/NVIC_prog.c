/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                                            */
/* Date: 09-Apr-19                                                */
/* Version: 01                                                */
/* Description: Program file for NVIC Driver         */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "NVIC_interface.h" 
#include "NVIC_priv.h" 
#include "NVIC_config.h" 
