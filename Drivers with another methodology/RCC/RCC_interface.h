#ifndef RCC_INTERFACE_H_
#define RCC_INTERFACE_H_

/*Macros for system clock definition*/
#define RCC_u8_HSI   (u8)0
#define RCC_u8_HSE   (u8)1
#define RCC_u8_PLL   (u8)2

/*Macros for the possible states (Enable/Disable)*/
#define RCC_u8_DISABLE           (u8)0
#define RCC_u8_ENABLE            (u8)1

/*
*  Macro to determine the PLL entry source (in case PLL is chosen as the system clock)
*  RCC_u8_HSI_DIVIDED_BY_2
*  RCC_u8_HSE_CLK_SOURCE
*/
#define RCC_u8_HSI_DIVIDED_BY_2    (u8)0
#define RCC_u8_HSE_CLK_SOURCE      (u8)1

/*Macros for PLL multiplication factor*/
#define  RCC_u8_PLL_MULTIPLED_BY_2     0b0000
#define  RCC_u8_PLL_MULTIPLED_BY_3     0b0001
#define  RCC_u8_PLL_MULTIPLED_BY_4     0b0010
#define	 RCC_u8_PLL_MULTIPLED_BY_5     0b0011
#define  RCC_u8_PLL_MULTIPLED_BY_6     0b0100
#define  RCC_u8_PLL_MULTIPLED_BY_7     0b0101
#define  RCC_u8_PLL_MULTIPLED_BY_8     0b0110
#define  RCC_u8_PLL_MULTIPLED_BY_9     0b0111
#define  RCC_u8_PLL_MULTIPLED_BY_10    0b1000
#define  RCC_u8_PLL_MULTIPLED_BY_11    0b1001
#define  RCC_u8_PLL_MULTIPLED_BY_12    0b1010
#define  RCC_u8_PLL_MULTIPLED_BY_13    0b1011
#define  RCC_u8_PLL_MULTIPLED_BY_14    0b1100
#define  RCC_u8_PLL_MULTIPLED_BY_15    0b1101
#define  RCC_u8_PLL_MULTIPLED_BY_16    0b1110

/*Macros for prescaler probable states*/
#define RCC_u8_NOT_DIVIDED            (u8)0
#define RCC_u8_DIVIDED_BY_2           (u8)1
#define RCC_u8_DIVIDED_BY_4           (u8)2
#define RCC_u8_DIVIDED_BY_8           (u8)3
#define RCC_u8_DIVIDED_BY_16          (u8)4
#define RCC_u8_DIVIDED_BY_64          (u8)5
#define RCC_u8_DIVIDED_BY_128         (u8)6
#define RCC_u8_DIVIDED_BY_256         (u8)7
#define RCC_u8_DIVIDED_BY_512         (u8)8

/*Macros for APB2 peripherals*/
#define RCC_u8_TIM11             (u8)1
#define RCC_u8_TIM10             (u8)2
#define RCC_u8_TIM9              (u8)3
#define RCC_u8_ADC3              (u8)4
#define RCC_u8_USART1            (u8)5
#define RCC_u8_TIM8              (u8)6
#define RCC_u8_SPI1              (u8)7
#define RCC_u8_TIM1              (u8)8
#define RCC_u8_ADC2              (u8)9
#define RCC_u8_ADC1              (u8)10
#define RCC_u8_IOPD              (u8)11
#define RCC_u8_IOPC              (u8)12
#define RCC_u8_IOPB              (u8)13
#define RCC_u8_IOPA              (u8)14
#define RCC_u8_AFIO              (u8)15

/*Macros for APB1 peripherals*/
#define RCC_u8_DAC               (u8)16
#define RCC_u8_PWR               (u8)17
#define RCC_u8_BKP               (u8)18
#define RCC_u8_CAN               (u8)19
#define RCC_u8_I2C2              (u8)20
#define RCC_u8_I2C1              (u8)21
#define RCC_u8_UART5             (u8)22
#define RCC_u8_UART4             (u8)23
#define RCC_u8_USART3            (u8)24
#define RCC_u8_USART2            (u8)25
#define RCC_u8_SPI3              (u8)26
#define RCC_u8_SPI2              (u8)27
#define RCC_u8_WWDG              (u8)28
#define RCC_u8_TIM14             (u8)29
#define RCC_u8_TIM13             (u8)30
#define RCC_u8_TIM12             (u8)31
#define RCC_u8_TIM7              (u8)32
#define RCC_u8_TIM6              (u8)33
#define RCC_u8_TIM5              (u8)34
#define RCC_u8_TIM4              (u8)35
#define RCC_u8_TIM3              (u8)36
#define RCC_u8_TIM2              (u8)37

/*Macros for AHB peripherals*/
#define RCC_u8_SDIO              (u8)38
#define RCC_u8_FSMC              (u8)39
#define RCC_u8_CRC               (u8)40
#define RCC_u8_FLITF             (u8)41
#define RCC_u8_SRAM              (u8)42
#define RCC_u8_DMA2              (u8)43
#define RCC_u8_DMA1              (u8)44

/*
 * Description: Function to set the PLL configurations
 * Inputs: the PLL entry source and the PLL multiplier
 * Output: Error state
 */
u8 RCC_u8ConfgPll (u8 Copy_u8PllSource,u8 Copy_u8PllMultiplier);

/*
 * Description: Function to set the state of the required clock
 * Inputs: the clock source and the required state
 * Output: Error state
 */
u8 RCC_u8SetClkCommand (u8 Copy_u8ClkSource,u8 Copy_u8State);

/*
 * Description: Function to select the System Clock Source
 * Inputs: the clock name
 * Output: Error state
 */
u8 RCC_u8SelectSysClk (u8 Copy_u8ClkSource);

/*
 * Description: Function to set the AHB prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8AHBPrescaler (u8 Copy_u8Prescaler);

/*
 * Description: Function to set the APB1 prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8APB1BPrescaler (u8 Copy_u8Prescaler);

/*
 * Description: Function to set the APB2 prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8APB2Prescaler (u8 Copy_u8Prescaler);

/*
 * Description: Function to set the state of any peripheral in APB1 (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8APB1PeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State);

/*
 * Description: Function to set the state of any peripheral in APB2 (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8APB2PeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State);

/*
 * Description: Function to set the state of any peripheral in AHB (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8AHBPeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State);


#endif /* RCC_INTERFACE_H_ */
