#include "STD_TYPES.h"
#include "RCC_interface.h"

/*Defining the RCC CR register bits */
#define HSION      (u8)0
#define HSIRDY     (u8)1
#define HSEON      (u8)16
#define HSERDY     (u8)17
#define PLLON      (u8)24
#define PLLRDY     (u8)25

/*Defining the RCC CFGR register bits */
#define SW_HSI      (u8)0
#define SW_HSE      (u8)1
#define SW_PLL      (u8)2
#define PLLSRC      (u8)16
#define PLLMUL      (u8)18
#define HPRE        (u8)4
#define PPRE1       (u8)8
#define PPRE2       (u8)11

/*Defining the RCC APB1 peripheral bits */
#define DACRST    (u8)29
#define PWR  	  (u8)28
#define BKP       (u8)27
#define CAN       (u8)25
#define USB       (u8)23
#define I2C2      (u8)22
#define I2C1      (u8)21
#define UART5     (u8)20
#define UART4     (u8)19
#define USART3    (u8)18
#define USART2    (u8)17
#define SPI3  	  (u8)15
#define SPI2      (u8)14
#define WWDG      (u8)11
#define TIM14     (u8)8
#define TIM13     (u8)7
#define TIM12     (u8)6
#define TIM7      (u8)5
#define TIM6      (u8)4
#define TIM5      (u8)3
#define TIM4      (u8)2
#define TIM3      (u8)1
#define TIM2      (u8)0

/*Defining the RCC APB2 peripheral bits */
#define TIM11     (u8)21
#define TIM10     (u8)20
#define TIM9      (u8)19
#define ADC3      (u8)15
#define USART1    (u8)14
#define TIM8      (u8)13
#define SPI1      (u8)12
#define TIM1      (u8)11
#define ADC2      (u8)10
#define ADC1      (u8)9
#define IOPD      (u8)5
#define IOPC      (u8)4
#define IOPB      (u8)3
#define IOPA      (u8)2
#define AFIO      (u8)0

/*Defining the RCC AHB peripheral bits */
#define SDIO      (u8)10
#define FSMC      (u8)8
#define CRC       (u8)6
#define FLITF     (u8)4
#define SRAM      (u8)2
#define DMA2      (u8)1
#define DMA1      (u8)0

#define RCC_AHB_DIVIDED_BY_2           0b1000
#define RCC_AHB_DIVIDED_BY_4           0b1001
#define RCC_AHB_DIVIDED_BY_8		   0b1010
#define RCC_AHB_DIVIDED_BY_16		   0b1011
#define RCC_AHB_DIVIDED_BY_64		   0b1100
#define RCC_AHB_DIVIDED_BY_128		   0b1101
#define RCC_AHB_DIVIDED_BY_256		   0b1110
#define RCC_AHB_DIVIDED_BY_512		   0b1111

#define RCC_APB_DIVIDED_BY_2           0b100
#define RCC_APB_DIVIDED_BY_4           0b101
#define RCC_APB_DIVIDED_BY_8		   0b110
#define RCC_APB_DIVIDED_BY_16		   0b111

/* STM32F103 RCC registers */
typedef struct
{
	u32 CR;
	u32 CFGR;
	u32 CIR;
	u32 APB2RSTR;
	u32 APB1RSTR;
	u32 AHBENR;
	u32 APB2ENR;
	u32 APB1ENR;
	u32 BDCR;
	u32 CSR;
	u32 AHBRSTR;
}RCC_REG;

#define  RCC   ((RCC_REG*)0x40021000)

/*
 * Description: Function to set the PLL configurations
 * Inputs: the PLL entry source and the PLL multiplier
 * Output: Error state
 */
u8 RCC_u8ConfgPll (u8 Copy_u8PllSource,u8 Copy_u8PllMultiplier)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	/* Checking the PLL entry source (in case PLL is chosen as the system clock) */
	switch (Copy_u8PllSource)
	{
		case RCC_u8_HSI_DIVIDED_BY_2:
		{
			RCC->CFGR &= ~(1<<PLLSRC);
			break;
		}
		case RCC_u8_HSE_CLK_SOURCE:
		{
			RCC->CFGR |= (1<<PLLSRC);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
		RCC->CFGR |= (Copy_u8PllMultiplier<<PLLMUL);
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the state of the required clock
 * Inputs: the clock source and the required state
 * Output: Error state
 */
u8 RCC_u8SetClkCommand (u8 Copy_u8ClkSource,u8 Copy_u8State)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	switch (Copy_u8ClkSource)
	{
		case RCC_u8_HSI:
		{
			/*	Checking if the selected HSI should be enabled*/
			if (Copy_u8State == RCC_u8_ENABLE)
			{
				/*Enable Internal high-speed clock in RCC_CR register */
				RCC->CR |= (1<<HSION);

				/*waiting HSI clock to be ready and stable*/
				while ((RCC->CR & (1<<HSIRDY)) == 0);
			}
			/* if the selected HSI should be disabled */
			else
			{
				/*Disable Internal high-speed clock in RCC_CR register */
				RCC->CR &= ~(1<<HSION);
			}
			break;
		}
		case RCC_u8_HSE:
		{
			/*	Checking if the selected HSE should be enabled*/
			if (Copy_u8State == RCC_u8_ENABLE)
			{
				/*Enable External high-speed clock in RCC_CR register */
				RCC->CR |= (1<<HSEON);

				/*waiting HSE clock to be ready and stable*/
				while ((RCC->CR & (1<<HSERDY)) == 0);
			}
			/* if the selected HSE should be disabled */
			else
			{
				/*Disable External high-speed clock in RCC_CR register */
				RCC->CR &= ~(1<<HSEON);
			}
			break;
		}
		case RCC_u8_PLL:
		{
			/*	Checking if the selected PLL should be enabled*/
			if (Copy_u8State == RCC_u8_ENABLE)
			{
				/*Enable PLL clock in RCC_CR register */
				RCC->CR |= (1<<PLLON);
				/*waiting PLL clock to be ready and stable*/
				while ((RCC->CR & (1<<HSERDY)) == 0);
			}
			/* if the selected PLL should be disabled */
			else
			{
				/*Disable PLL clock in RCC_CR register */
				RCC->CR &= ~(1<<PLLON);
			}
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to select the System Clock Source
 * Inputs: the clock name
 * Output: Error state
 */
u8 RCC_u8SelectSysClk (u8 Copy_u8ClkSource)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	switch (Copy_u8ClkSource)
	{
	    /* Checking if HSI is the selected Clock Source*/
		case RCC_u8_HSI:
		{
			/* Select HSI as the system clock in RCC_CFGR register */
			RCC->CFGR &= ~(3<<0);
			break;
		}
		/* Checking if HSE is the selected Clock Source */
		case RCC_u8_HSE:
		{
			/* Select HSE as the system clock in RCC_CFGR register */
			RCC->CFGR &= ~(3<<0);
			RCC->CFGR |= SW_HSE;
			break;
		}
		/* Checking if PLL is the selected Clock Source */
		case RCC_u8_PLL:
		{
			/*Select PLL as the system clock in RCC_CFGR register */
			RCC->CFGR &= ~(3<<0);
			RCC->CFGR |= SW_PLL;
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the AHB prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8AHBPrescaler (u8 Copy_u8Prescaler)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	RCC->CFGR &= ~(0xF<<4);

	/*Checking the AHB prescaler*/
	switch (Copy_u8Prescaler)
	{
		case (RCC_u8_NOT_DIVIDED):
		{
			RCC->CFGR &= ~(1<<7);
			break;
		}
		case (RCC_u8_DIVIDED_BY_2):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_2<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_4):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_4<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_8):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_8<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_16):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_16<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_64):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_64<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_128):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_128<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_256):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_256<<HPRE);
			break;
		}
		case (RCC_u8_DIVIDED_BY_512):
		{
			RCC->CFGR |= (RCC_AHB_DIVIDED_BY_512<<HPRE);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the APB1 prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8APB1BPrescaler (u8 Copy_u8Prescaler)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	RCC->CFGR &= ~(0x7<<8);

	/*Checking the AHB prescaler*/
	switch (Copy_u8Prescaler)
	{
		case (RCC_u8_NOT_DIVIDED):
		{
			RCC->CFGR &= ~(1<<10);
			break;
		}
		case (RCC_u8_DIVIDED_BY_2):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_2<<PPRE1);
			break;
		}
		case (RCC_u8_DIVIDED_BY_4):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_4<<PPRE1);
			break;
		}
		case (RCC_u8_DIVIDED_BY_8):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_8<<PPRE1);
			break;
		}
		case (RCC_u8_DIVIDED_BY_16):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_16<<PPRE1);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the APB2 prescaler
 * Inputs: the required prescaler
 * Output: Error state
 */
u8 RCC_u8APB2Prescaler (u8 Copy_u8Prescaler)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	RCC->CFGR &= ~(0x7<<11);

	/*Checking the AHB prescaler*/
	switch (Copy_u8Prescaler)
	{
		case (RCC_u8_NOT_DIVIDED):
		{
			RCC->CFGR &= ~(1<<10);
			break;
		}
		case (RCC_u8_DIVIDED_BY_2):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_2<<PPRE2);
			break;
		}
		case (RCC_u8_DIVIDED_BY_4):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_4<<PPRE2);
			break;
		}
		case (RCC_u8_DIVIDED_BY_8):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_8<<PPRE2);
			break;
		}
		case (RCC_u8_DIVIDED_BY_16):
		{
			RCC->CFGR |= (RCC_APB_DIVIDED_BY_16<<PPRE2);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the state of any peripheral in APB1 (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8APB1PeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	/*Checking the APB1 prescaler*/
	switch (Copy_u8Peripheral)
	{
		case RCC_u8_DAC:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<DACRST);
			else
				RCC->APB1RSTR = (1<<DACRST);
			break;
		}
		case RCC_u8_PWR:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<PWR);
			else
				RCC->APB1RSTR = (1<<PWR);
			break;
		}
		case RCC_u8_BKP:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<BKP);
			else
				RCC->APB1RSTR = (1<<BKP);
			break;
		}
		case RCC_u8_CAN:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<CAN);
			else
				RCC->APB1RSTR = (1<<CAN);
			break;
		}
		case RCC_u8_I2C2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<I2C2);
			else
				RCC->APB1RSTR = (1<<I2C2);
			break;
		}
		case RCC_u8_I2C1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<I2C1);
			else
				RCC->APB1RSTR = (1<<I2C1);
			break;
		}
		case RCC_u8_UART5:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<UART5);
			else
				RCC->APB1RSTR = (1<<UART5);
			break;
		}
		case RCC_u8_UART4:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<UART4);
			else
				RCC->APB1RSTR = (1<<UART4);
			break;
		}
		case RCC_u8_USART3:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<USART3);
			else
				RCC->APB1RSTR = (1<<USART3);
			break;
		}
		case RCC_u8_USART2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<USART2);
			else
				RCC->APB1RSTR = (1<<USART2);
			break;
		}
		case RCC_u8_SPI3:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<SPI3);
			else
				RCC->APB1RSTR = (1<<SPI3);
			break;
		}
		case RCC_u8_SPI2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<SPI2);
			else
				RCC->APB1RSTR = (1<<SPI2);
			break;
		}
		case RCC_u8_WWDG:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<WWDG);
			else
				RCC->APB1RSTR = (1<<WWDG);
			break;
		}
		case RCC_u8_TIM7:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM7);
			else
				RCC->APB1RSTR = (1<<TIM7);
			break;
		}
		case RCC_u8_TIM6:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM6);
			else
				RCC->APB1RSTR = (1<<TIM6);
			break;
		}
		case RCC_u8_TIM5:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM5);
			else
				RCC->APB1RSTR = (1<<TIM5);
			break;
		}
		case RCC_u8_TIM4:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM4);
			else
				RCC->APB1RSTR = (1<<TIM4);
			break;
		}
		case RCC_u8_TIM3:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM3);
			else
				RCC->APB1RSTR = (1<<TIM3);
			break;
		}
		case RCC_u8_TIM2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB1ENR = (1<<TIM2);
			else
				RCC->APB1RSTR = (1<<TIM2);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the state of any peripheral in APB2 (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8APB2PeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	/*Checking the APB2 prescaler*/
	switch (Copy_u8Peripheral)
	{
		case RCC_u8_TIM11:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<TIM11);
			else
				RCC->APB2RSTR = (1<<TIM11);
			break;
		}
		case RCC_u8_TIM10:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<TIM10);
			else
				RCC->APB2RSTR = (1<<TIM10);
			break;
		}
		case RCC_u8_TIM9:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<TIM9);
			else
				RCC->APB2RSTR = (1<<TIM9);
			break;
		}
		case RCC_u8_ADC3:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<ADC3);
			else
				RCC->APB2RSTR = (1<<ADC3);
			break;
		}
		case RCC_u8_USART1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<USART1);
			else
				RCC->APB2RSTR = (1<<USART1);
			break;
		}
		case RCC_u8_TIM8:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<TIM8);
			else
				RCC->APB2RSTR = (1<<TIM8);
			break;
		}
		case RCC_u8_SPI1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<SPI1);
			else
				RCC->APB2RSTR = (1<<SPI1);
			break;
		}
		case RCC_u8_TIM1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<TIM1);
			else
				RCC->APB2RSTR = (1<<TIM1);
			break;
		}
		case RCC_u8_ADC2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<ADC2);
			else
				RCC->APB2RSTR = (1<<ADC2);
			break;
		}
		case RCC_u8_ADC1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<ADC1);
			else
				RCC->APB2RSTR = (1<<ADC1);
			break;
		}
		case RCC_u8_IOPD:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<IOPD);
			else
				RCC->APB2RSTR = (1<<IOPD);
			break;
		}
		case RCC_u8_IOPC:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<IOPC);
			else
				RCC->APB2RSTR = (1<<IOPC);
			break;
		}
		case RCC_u8_IOPB:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<IOPB);
			else
				RCC->APB2RSTR = (1<<IOPB);
			break;
		}
		case RCC_u8_IOPA:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<IOPA);
			else
				RCC->APB2RSTR = (1<<IOPA);
			break;
		}
		case RCC_u8_AFIO:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<AFIO);
			else
				RCC->APB2RSTR = (1<<AFIO);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}

/*
 * Description: Function to set the state of any peripheral in AHB (Enable/Disable)
 * Inputs: the peripheral name , and the required state
 * Output: Error state
 */
u8 RCC_u8AHBPeripheralsCommand (u8 Copy_u8Peripheral,u8 Copy_u8State)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	/*Checking the AHB prescaler*/
	switch (Copy_u8Peripheral)
	{
		case RCC_u8_SDIO:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<SDIO);
			else
				RCC->APB2RSTR = (1<<SDIO);
			break;
		}
		case RCC_u8_FSMC:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<FSMC);
			else
				RCC->APB2RSTR = (1<<FSMC);
			break;
		}
		case RCC_u8_CRC:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<CRC);
			else
				RCC->APB2RSTR = (1<<CRC);
			break;
		}
		case RCC_u8_FLITF:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<FLITF);
			else
				RCC->APB2RSTR = (1<<FLITF);
			break;
		}
		case RCC_u8_SRAM:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<SRAM);
			else
				RCC->APB2RSTR = (1<<SRAM);
			break;
		}
		case RCC_u8_DMA2:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<DMA2);
			else
				RCC->APB2RSTR = (1<<DMA2);
			break;
		}
		case RCC_u8_DMA1:
		{
			if (Copy_u8State == RCC_u8_ENABLE)
				RCC->APB2ENR = (1<<DMA1);
			else
				RCC->APB2RSTR = (1<<DMA1);
			break;
		}
		default:
		{
			Local_u8Error = ERROR_NOK;
			break;
		}
	}
	/* Function return */
	return Local_u8Error;
}


