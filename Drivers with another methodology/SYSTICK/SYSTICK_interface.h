/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                          */
/* Date: 23-Apr-19                                        */
/* Version: 01                                            */
/* Description: Interface file for SYSTICK Driver         */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef SYSTICK_INTERFACE_H 
#define SYSTICK_INTERFACE_H 

/* Macro to define the AHB clock frequency which is the source of Systick Timer */
#define SYSTICK_u8_AHB_FREQ   (u32)8000000

/*
 Macro to select the clock source of the Systick Timer
 Ranges:	SYSTICK_u8_AHB
		    SYSTICK_u8_AHB_OVER_8
*/
#define SYSTICK_u8_CLKSOURCE  SYSTICK_u8_AHB_OVER_8

/*
 Macro to select the initialized state of Systick Timer Interrupt
 Ranges:	SYSTICK_u8_ENABLE_EXEP_REQ
		    SYSTICK_u8_DISABLE_EXEP_REQ
*/
#define SYSTICK_u8_TICKINT    SYSTICK_u8_ENABLE_EXEP_REQ

/*Macro to define a pointer to function as a specific datatype */
typedef void (*CallBack_t) (void);

/*
 * Description: Function to set the initialized configuration in the SysTick control and status register (STK_CTRL)
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vInit(void);

/*
 * Description: Function to enable Systick Timer
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vEnable (void);

/*
 * Description: Function to disable Systick Timer
 * Inputs: none
 * Output: void
 */
extern void SYSTICK_vDisable (void);

/*
 * Description: Function to set the time needed between each Systick Interrupt
 * Inputs: The needed time in millisecond
 * Output: void
 */
extern void SYSTICK_vSetTickTime (u16 Copy_TimeMs);

/*
 * Description: Function to set the initialized configuration in t
 * Inputs: none
 * Output: the Error state of the function
 */
extern void SYSTICK_vSetCallBack (CallBack_t Func);

#endif /* SYSTICK_INTERFACE_H */ 
