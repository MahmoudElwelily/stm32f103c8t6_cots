/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 23-Apr-19                                            */
/* Version: 01                                                */
/* Description: Program file for SYSTICK Driver               */
/**************************************************************/
#include "STD_TYPES.h"
#include "SYSTICK_interface.h" 

/*Macros for the possible states for the Systick Timer (Enabled/Disabled)*/
#define SYSTICK_u8_EN                    0UL
#define SYSTICK_u8_DISABLE               1UL

/*Macros for the possible clock frequencies that can feed the Systick Peripheral*/
#define SYSTICK_u8_AHB                  (u8)0
#define SYSTICK_u8_AHB_OVER_8           (u8)1

/*Macros for the possible states for the SysTick exception request*/
#define SYSTICK_u8_DISABLE_EXEP_REQ		  (u8)0
#define SYSTICK_u8_ENABLE_EXEP_REQ      (u8)1

typedef struct
{
	u32 STK_CTRL;
	u32 STK_LOAD;
	u32 STK_VAL;
	u32 STK_CALIB;
}SYSTICK;
#define SYSTICK_TIMER          ((SYSTICK*)0xE000E010)

/*Defining the STK_CTRL register bits */
#define ENABLE      (u8)0
#define TICKINT     (u8)1
#define CLKSOURCE   (u8)2
#define COUNTFLAG   (u8)16

static CallBack_t SYSTICK_CallbackPtrFun = NULL;

/*
 * Description: Function to set the initialized configuration in the SysTick control and status register (STK_CTRL)
 * Inputs: none
 * Output: void
 */
void SYSTICK_vInit(void)
{
	if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
	{
		SYSTICK_TIMER->STK_CTRL |= (1 << CLKSOURCE);
	}
	else if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB_OVER_8)
	{
		SYSTICK_TIMER->STK_CTRL &= ~(1 << CLKSOURCE);
	}

	if (SYSTICK_u8_TICKINT == SYSTICK_u8_DISABLE_EXEP_REQ)
	{
		SYSTICK_TIMER->STK_CTRL &= ~(1 << TICKINT);
	}
	else if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_ENABLE_EXEP_REQ)
	{
		SYSTICK_TIMER->STK_CTRL |= (1 << TICKINT);
	}
	return;
}

/*
 * Description: Function to enable Systick Timer
 * Inputs: none
 * Output: void
 */
void SYSTICK_vEnable (void)
{
	SYSTICK_TIMER->STK_CTRL |= (1 << ENABLE);
	return;
}

/*
 * Description: Function to disable Systick Timer
 * Inputs: none
 * Output: void
 */
void SYSTICK_vDisable (void)
{
	SYSTICK_TIMER->STK_CTRL &= ~(1 << ENABLE);
	return;
}

/*
 * Description: Function to set the time needed between each Systick Interrupt
 * Inputs: The needed time in millisecond
 * Output: void
 */
void SYSTICK_vSetTickTime (u16 Copy_TimeMs)
{
	f32 Local_f32TickTime = 0;

	/* if the Systick Timer takes the AHB clock frequency without prescaler */
	if (SYSTICK_u8_CLKSOURCE == SYSTICK_u8_AHB)
	{
		/* The Systick Time in millisecond */
		Local_f32TickTime = (1.0/SYSTICK_u8_AHB_FREQ)*1000;
	}
	/* if the Systick Timer takes the AHB clock frequency over 8 */
	else
	{
		/* The Systick Time in millisecond */
		Local_f32TickTime = (8.0/SYSTICK_u8_AHB_FREQ)*1000;
	}
	SYSTICK_TIMER->STK_LOAD = (u32)(Copy_TimeMs/Local_f32TickTime);
	return;
}

/*
 * Description: Function to set the initialized configuration in t
 * Inputs: none
 * Output: the Error state of the function
 */
void SYSTICK_vSetCallBack (CallBack_t CallbackFunc)
{
	if (CallbackFunc != NULL)
	{
		SYSTICK_CallbackPtrFun = CallbackFunc;
	}
	return;
}

void SysTick_Handler (void)
{
	if (SYSTICK_CallbackPtrFun != NULL)
	{
		SYSTICK_CallbackPtrFun();
	}
	return;
}
