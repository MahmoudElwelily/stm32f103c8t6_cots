#include "STD_TYPES.h"
#include "GPIO_interface.h"

/* Definition of the available number of ports */
#define NO_OF_PORT               (u8)4
/* Definition of the number of pins in each port */
#define NO_OF_PINS               (u8)16
/* Definition of the number of configured pins on the CRL/CRH registers */
#define NO_OF_CRL_CONFG_BITS     (u8)8

/* Direction and Modes Macros for the DIO pins */
#define DIO_u8_INPUT_ANALOG                       0b0000
#define DIO_u8_INPUT_FLOATING                     0b0100
#define DIO_u8_INPUT_PULL_UP                      0b1000
#define DIO_u8_INPUT_PULL_DOWN					  0b1000

#define DIO_u8_OUPUT_10M_PUSHPULL				  0b0001
#define DIO_u8_OUPUT_10M_OPENDRAIN			      0b0101
#define DIO_u8_OUPUT_10M_ALTFUN_PUSHPULL          0b1001
#define DIO_u8_OUPUT_10M_ALTFUN_OPENDRAIN         0b1101

#define DIO_u8_OUPUT_2M_PUSHPULL                  0b0010
#define DIO_u8_OUPUT_2M_OPENDRAIN				  0b0110
#define DIO_u8_OUPUT_2M_ALTFUN_PUSHPULL           0b1010
#define DIO_u8_OUPUT_2M_ALTFUN_OPENDRAIN          0b1110

#define DIO_u8_OUPUT_50M_PUSHPULL				  0b0011
#define DIO_u8_OUPUT_50M_OPENDRAIN		          0b0111
#define DIO_u8_OUPUT_50M_ALTFUN_PUSHPULL	      0b1011
#define DIO_u8_OUPUT_50M_ALTFUN_OPENDRAIN         0b1111

/**********************************************************/
/*                 Register Definitions                   */
/**********************************************************/
#define PORTA_BASE_ADDRESS     (GPIO_Registers *)0x40010800
#define PORTB_BASE_ADDRESS     (GPIO_Registers *)0x40010C00
#define PORTC_BASE_ADDRESS     (GPIO_Registers *)0x40011000
#define PORTD_BASE_ADDRESS     (GPIO_Registers *)0x40014000

typedef struct
{
	u32 GPIOx_CRL;
	u32 GPIOx_CRH;
	u32 GPIOx_IDR;
	u32 GPIOx_ODR;
	u32 GPIOx_BSRR;
	u32 GPIOx_BRR;
	u32 GPIOx_LCKR;
} GPIO_Registers;

/* Array of pointer to structure holding the base address of each port */
GPIO_Registers *const PortsBaseAddresses[NO_OF_PORT] =
{
		PORTA_BASE_ADDRESS,
		PORTB_BASE_ADDRESS,
		PORTC_BASE_ADDRESS,
		PORTD_BASE_ADDRESS
};

/**********************************************************/
/*                 Main GPIO Functions                    */
/**********************************************************/
/*
 * Description: Function to set the (Direction-Mode-Speed) of the assigned pin
 * Inputs: structure holding the pin configurations
 * Output: Error state
 */
u8 GPIO_u8SetPinConfiguration (GPIO_Pin_t* PinStruct)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u8 Local_u8Mode = UNSIGNED_MIN;

	if ( (PinStruct->PortID < NO_OF_PORT) && (PinStruct->PinNumber < NO_OF_PINS) && (PinStruct != NULL) )
	{
		/* If the pin is configured as input pin */
		if (PinStruct->Direction == GDIO_u8_INPUT)
		{
			switch (PinStruct->Mode)
			{
				case GDIO_u8_ANALOG:
				{
					Local_u8Mode = DIO_u8_INPUT_ANALOG;
					break;
				}
				case GDIO_u8_FLOATING:
				{
					Local_u8Mode = DIO_u8_INPUT_FLOATING;
					break;
				}
				case GDIO_u8_PULL_UP:
				{
					Local_u8Mode = DIO_u8_INPUT_PULL_UP;
					break;
				}
				case GDIO_u8_PULL_DOWN:
				{
					Local_u8Mode = DIO_u8_INPUT_PULL_DOWN;
					break;
				}
			}
			/* Checking if the input pin is configured as input pull up */
			if (PinStruct->Mode == GDIO_u8_PULL_UP)
			{
				PortsBaseAddresses[PinStruct->PortID]->GPIOx_BSRR = (1<<PinStruct->PinNumber);
			}
			/* Checking if the input pin is configured as input pull down */
			else
			{
				PortsBaseAddresses[PinStruct->PortID]->GPIOx_BRR = (1<<PinStruct->PinNumber);
			}
		}
		/* If the pin is configured as output pin */
		else
		{
			switch (PinStruct->Mode)
			{
				case GDIO_u8_PUSHPULL:
				{
					if (PinStruct->Frequency == GDIO_u8_FREQ_2_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_2M_PUSHPULL;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_10_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_10M_PUSHPULL;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_50_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_50M_PUSHPULL;
					break;
				}
				case GDIO_u8_OPENDRAIN:
				{
					if (PinStruct->Frequency == GDIO_u8_FREQ_2_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_2M_OPENDRAIN;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_10_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_10M_OPENDRAIN;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_50_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_50M_OPENDRAIN;
					break;
				}
				case GDIO_u8_ALTFUN_PUSHPULL:
				{
					if (PinStruct->Frequency == GDIO_u8_FREQ_2_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_2M_ALTFUN_PUSHPULL;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_10_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_10M_ALTFUN_PUSHPULL;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_50_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_50M_ALTFUN_PUSHPULL;
					break;
				}
				case GDIO_u8_ALTFUN_OPENDRAIN:
				{
					if (PinStruct->Frequency == GDIO_u8_FREQ_2_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_2M_ALTFUN_OPENDRAIN;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_10_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_10M_ALTFUN_OPENDRAIN;
					else if (PinStruct->Frequency == GDIO_u8_FREQ_50_MHZ)
						Local_u8Mode = DIO_u8_OUPUT_50M_ALTFUN_OPENDRAIN;
					break;
				}
			}
		}

		/* Setting the proper mode for the required pin */
		if (PinStruct->PinNumber < NO_OF_CRL_CONFG_BITS)
		{
			PortsBaseAddresses[PinStruct->PortID]->GPIOx_CRL &= ~((u32)(0xF<<4*PinStruct->PinNumber));
			PortsBaseAddresses[PinStruct->PortID]->GPIOx_CRL |= (u32)(Local_u8Mode<<4*PinStruct->PinNumber);
		}
		else
		{
			PortsBaseAddresses[PinStruct->PortID]->GPIOx_CRH &= ~((u32)(Local_u8Mode<<4*(PinStruct->PinNumber-NO_OF_CRL_CONFG_BITS)));
			PortsBaseAddresses[PinStruct->PortID]->GPIOx_CRH |= (Local_u8Mode<<4*(PinStruct->PinNumber-NO_OF_CRL_CONFG_BITS));
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: structure holding the pin number and the required state
 * Output: Error state
 */
u8 GPIO_u8SetPinValue (GPIO_Pin_t* PinStruct,u8 Copy_u8State)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	if ( (PinStruct->PortID < NO_OF_PORT) && (PinStruct->PinNumber < NO_OF_PINS) )
	{
		switch(Copy_u8State)
		{
			case GPIO_u8_HIGH:
			{
				PortsBaseAddresses[PinStruct->PortID]->GPIOx_BSRR = (1 << PinStruct->PinNumber);
				break;
			}
			case GPIO_u8_LOW:
			{
				PortsBaseAddresses[PinStruct->PortID]->GPIOx_BRR = (1 << PinStruct->PinNumber);
				break;
			}
		}
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: structure holding  the pin number and the container which will hold the value of the pin
 * Output: Error state
 */
u8 GPIO_u8GetPinValue (GPIO_Pin_t* PinStruct, u8* Copy_u8PinValue)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	if ( (PinStruct->PortID < NO_OF_PORT) && (PinStruct->PinNumber< NO_OF_PINS) )
	{
		if ( (PortsBaseAddresses[PinStruct->PortID]->GPIOx_IDR) & (1 << PinStruct->PinNumber) )
			* Copy_u8PinValue = GPIO_u8_HIGH;
		else
			* Copy_u8PinValue = GPIO_u8_LOW;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

