#ifndef GPIO_INTERFACE_H_
#define GPIO_INTERFACE_H_

/* Defining the DIO pins */
#define   GPIO_u8_PIN0        		 (u8)0
#define   GPIO_u8_PIN1       	 	 (u8)1
#define   GPIO_u8_PIN2       	 	 (u8)2
#define   GPIO_u8_PIN3       	 	 (u8)3
#define   GPIO_u8_PIN4       	 	 (u8)4
#define   GPIO_u8_PIN5       		 (u8)5
#define   GPIO_u8_PIN6        		 (u8)6
#define   GPIO_u8_PIN7        		 (u8)7
#define   GPIO_u8_PIN8       	 	 (u8)8
#define   GPIO_u8_PIN9        		 (u8)9
#define   GPIO_u8_PIN10       		 (u8)10
#define   GPIO_u8_PIN11      		 (u8)11
#define   GPIO_u8_PIN12      		 (u8)12
#define   GPIO_u8_PIN13      		 (u8)13
#define   GPIO_u8_PIN14      		 (u8)14
#define   GPIO_u8_PIN15       		 (u8)15

/* Defining the available ports */
#define   GPIO_u8_PORTA   			 (u8)0
#define   GPIO_u8_PORTB   			 (u8)1
#define   GPIO_u8_PORTC	 			 (u8)2
#define   GPIO_u8_PORTD    			 (u8)3

/* Defining the Possible directions for the DIO pin */
#define   GDIO_u8_INPUT              (u8)0
#define   GDIO_u8_OUTPUT 			 (u8)1

/* Defining the Output modes */
#define   GDIO_u8_PUSHPULL           (u8)0
#define   GDIO_u8_OPENDRAIN			 (u8)1
#define   GDIO_u8_ALTFUN_PUSHPULL    (u8)2
#define   GDIO_u8_ALTFUN_OPENDRAIN   (u8)3

/* Defining the Input modes */
#define   GDIO_u8_ANALOG             (u8)0
#define   GDIO_u8_FLOATING           (u8)1
#define   GDIO_u8_PULL_UP		     (u8)2
#define   GDIO_u8_PULL_DOWN          (u8)3

/* Defining the possible frequencies for the output pin */
#define   GDIO_u8_FREQ_2_MHZ         (u8)0
#define   GDIO_u8_FREQ_10_MHZ		 (u8)1
#define   GDIO_u8_FREQ_50_MHZ        (u8)2

/* Defining the Possible output voltage on the DIO pin */
#define   GPIO_u8_LOW     			 (u8)0
#define   GPIO_u8_HIGH               (u8)1

/* Definition of the pin structure which will hold the multiple pn configurations (Pin number - Port number - Drection- Mode - Frequency)*/
typedef struct
{
	u8 PinNumber;
	u8 PortID;
	u8 Direction;
	u8 Mode;
	u8 Frequency;
} GPIO_Pin_t;

/**********************************************************/
/*                 Main GPIO Functions                    */
/**********************************************************/
/*
 * Description: Function to set the (Direction-Mode-Speed) of the assigned pin
 * Inputs: structure holding the pin configurations
 * Output: Error state
 */
u8 GPIO_u8SetPinConfiguration (GPIO_Pin_t* PinStruct);
/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: structure holding the pin number and the required state
 * Output: Error state
 */
u8 GPIO_u8SetPinValue (GPIO_Pin_t* PinStruct,u8 Copy_u8State);
/*
 * Description: Function to set the state of any peripheral (Enable/disable)
 * Inputs: structure holding  the pin number and the container which will hold the value of the pin
 * Output: Error state
 */
u8 GPIO_u8GetPinValue (GPIO_Pin_t* PinStruct, u8* Copy_u8PinValue );

#endif /* GPIO_INTERFACE_H_ */
