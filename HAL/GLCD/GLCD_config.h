/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 16-May-19                                            */
/* Version: 01                                                */
/* Description: Configuration file for GLCD Driver            */
/**************************************************************/

/*Preprocessor Guard*/
#ifndef GLCD_CONFIG_H 
#define GLCD_CONFIG_H 

/* Macro to specify the DC pin */
#define GLCD_u8_DC_PIN      DIO_u8_PIN24

/* Macro to specify the CE pin */
#define GLCD_u8_CE_PIN      DIO_u8_PIN25

/* Macro to specify the RST pin */
#define GLCD_u8_RST_PIN     DIO_u8_PIN26

#endif /* GLCD_CONFIG_H */ 

