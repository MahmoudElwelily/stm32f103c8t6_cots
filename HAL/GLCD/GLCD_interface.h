/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                          */
/* Date: 16-May-19                                        */
/* Version: 01                                            */
/* Description: Interface file for GLCD Driver            */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef GLCD_INTERFACE_H 
#define GLCD_INTERFACE_H 

/* Function Name : GLCD_vInit
 * Description: initializes the graphical LCD parameters
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vInit(void);

/* Function Name : GLCD_vWriteData
 * Description: function to send data to the graphical LCD parameters
 * Inputs     : the data needed to be sent
 * Outputs    : None
 */
void GLCD_vWriteData (u8 Copy_u8Data);

/* Function Name : GLCD_vClear
 * Description: function to clear the graphical LCD
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vClear(void);

/* Function Name : GLCD_vSetXY
 * Description: function to set the X and Y address of RAM of the GLCD
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vSetXY (u8 Copy_u8LocX,u8 Copy_u8LocY);

/* Function Name : GLCD_vDisplayImage
 * Description: function to set the X and Y address of RAM of the GLCD
 * Inputs     : pointer to the array of the image
 * Outputs    : None
 */
void GLCD_vDisplayImage(const unsigned char *image_data);

#endif /* GLCD_INTERFACE_H */ 
