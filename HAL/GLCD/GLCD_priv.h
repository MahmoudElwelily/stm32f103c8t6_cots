/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                          */
/* Date: 16-May-19                                        */
/* Version: 01                                            */
/* Description: Private file for GLCD Driver              */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef GLCD_PRIV_H 
#define GLCD_PRIV_H 

/* Definition of the main GLCD instructions */
#define GLCD_EXT_INST     0b00100001
#define GLCD_ADJ_CONST    0b10111110
#define GLCD_ADJ_BIAS     0b00010011
#define GLCD_ADJ_TEMP     0b00000110
#define GLCD_BASIC_INST   0b00100000
#define GLCD_NORM_DSP     0b00001100

/* Function Name : GLCD_vWriteCommand
 * Description: function to send commands to the graphical LCD parameters
 * Inputs     : the command needed to be sent
 * Outputs    : None
 */
static void GLCD_vWriteCommand (u8 Copy_u8Command);

#endif /* GLCD_PRIV_H */ 
