/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 16-May-19                                            */
/* Version: 01                                                */
/* Description: Program file for GLCD Driver                  */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "DELAY_interface.h"
#include "GLCD_interface.h" 
#include "GLCD_priv.h" 
#include "GLCD_config.h" 
#include "DIO_interface.h"
#include "SPI_interface.h"

/* Function Name : GLCD_vInit
 * Description: initializes the graphical LCD parameters
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vInit(void)
{
	DIO_u8SetPinValue (GLCD_u8_RST_PIN,DIO_u8_PIN_LOW);
	delay_milliseconds (100);
	DIO_u8SetPinValue (GLCD_u8_RST_PIN,DIO_u8_PIN_HIGH);
	delay_milliseconds (100);

	/* Extended Instruction Set */
	GLCD_vWriteCommand (GLCD_EXT_INST);

	/* Adjust the contrast */
	GLCD_vWriteCommand (GLCD_ADJ_CONST);

	/* Adjust temperature compensation */
	GLCD_vWriteCommand (GLCD_ADJ_TEMP);

	/* Adjust bias levels */
	GLCD_vWriteCommand (GLCD_ADJ_BIAS);

	/* Basic Instruction Set */
	GLCD_vWriteCommand (GLCD_BASIC_INST);

	/* Normal mode Display */
	GLCD_vWriteCommand (GLCD_NORM_DSP);

	GLCD_vWriteCommand (0b10000000);
	GLCD_vWriteCommand (0b01000000);
}


/* Function Name : GLCD_vWriteCommand
 * Description: function to send commands to the graphical LCD
 * Inputs     : the command needed to be sent
 * Outputs    : None
 */
static void GLCD_vWriteCommand (u8 Copy_u8Command)
{
	u8 Local_u8Temp;
	/* make DC pin to logic zero for command operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_LOW);
	/* enable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_LOW);

	SPI_u8SendReceiveByte_Synch (Copy_u8Command,&Local_u8Temp);

	/* disable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_HIGH);

	/* make DC pin to logic zero for data operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_HIGH);

	return;
}

/* Function Name : GLCD_vWriteData
 * Description: function to send data to the graphical LCD
 * Inputs     : the data needed to be sent
 * Outputs    : None
 */
void GLCD_vWriteData (u8 Copy_u8Data)
{
	u8 Local_u8Temp;

	/* make DC pin to logic high for data operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_HIGH);
	/* enable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_LOW);

	SPI_u8SendReceiveByte_Synch (Copy_u8Data,&Local_u8Temp);

	/* disable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_HIGH);

	/* make DC pin to logic zero for command operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_LOW);

	return;
}

/* Function Name : GLCD_vClear
 * Description: function to clear the graphical LCD
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vClear (void) 	/* clear the Display */
{
	u16 k;
	u8 Local_u8Temp;
	/* make DC pin to logic high for data operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_HIGH);
	/* enable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_LOW);

	for (k = 0; k <= 503; k++)
	{
		SPI_u8SendReceiveByte_Synch (0x00,&Local_u8Temp);
	}

	/* disable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_HIGH);

	/* make DC pin to logic zero for command operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_LOW);
	return;
}

/* Function Name : GLCD_vSetXY
 * Description: function to set the X and Y address of RAM of the GLCD
 * Inputs     : None
 * Outputs    : None
 */
void GLCD_vSetXY (u8 Copy_u8LocX,u8 Copy_u8LocY)
{
	GLCD_vWriteCommand (Copy_u8LocX);
	GLCD_vWriteCommand (Copy_u8LocY);
	return;
}

/* Function Name : GLCD_vDisplayImage
 * Description: function to set the X and Y address of RAM of the GLCD
 * Inputs     : pointer to the array of the image
 * Outputs    : None
 */
void GLCD_vDisplayImage(const unsigned char *image_data)
{
	u16 k;
	u8 Local_u8Temp;

	/* make DC pin to logic high for data operation */
	DIO_u8SetPinValue (GLCD_u8_DC_PIN,DIO_u8_PIN_HIGH);
	/* enable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_LOW);

	for (k=0; k <= 503; k++)
	{
		SPI_u8SendReceiveByte_Synch (image_data[k],&Local_u8Temp);
	}

	/* disable SS pin to slave selection */
	DIO_u8SetPinValue (GLCD_u8_CE_PIN,DIO_u8_PIN_HIGH);

	return;
}

