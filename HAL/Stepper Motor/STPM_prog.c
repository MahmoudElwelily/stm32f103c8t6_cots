/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 16-Mar-19                                            */
/* Version: 01                                                */
/* Description: Program file for Stepper Motor Driver         */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "DELAY_interface.h"
#include "DIO_interface.h"
#include "STPM_interface.h" 
#include "STPM_priv.h" 
#include "STPM_config.h" 

/*
 * Description: Function to set the angle of the assigned stepper motor to a required angle within a desired time
 * Inputs: The stepper Number, the desired angle, the required direction, and the rotation desired time (speed)
 * Output: the error state
 */
u8 STPM_u8Rotate (u8 Copy_u8StepperId,u32 Copy_u8Angle,u8 Copy_u8Direction,u16 Copy_u16TimeInMs)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16Index;

	/* Local variable to hold the minimum angle according to the motor coil number*/
	f32 Local_f32Step = 360 /(STPM_ASu8StpmArray[Copy_u8StepperId].CoilNumber*1.0);

	/*Local variable holding the number of steps needed to reach the required angle */
	u16 Local_u16NoOfSteps = (u16)(Copy_u8Angle/Local_f32Step);

	/*Local varable holding the time needed to rotate from a step to another according to the required rotation time*/
	u16 Copy_u16StepDelay = Copy_u16TimeInMs/Local_u16NoOfSteps;

	if (Copy_u8StepperId >= STMP_NO_OF_STPM)
		 Local_u8Error = ERROR_NOK;
	else
	{
		if (STPM_ASu8StpmArray[Copy_u8StepperId].ActivationType == STPM_u8_ACTIVE_LOW)
		{
			for (Local_u16Index = UNSIGNED_MIN ; Local_u16Index < Local_u16NoOfSteps/4 ; Local_u16Index++)
			{
				STPM_vSetActiveLowSequence (Copy_u8StepperId,Copy_u8Direction,Copy_u16StepDelay);
			}
		}
		else
		{
			for (Local_u16Index = UNSIGNED_MIN ; Local_u16Index < Local_u16NoOfSteps/4 ; Local_u16Index++)
			{
				STPM_vSetActiveHighSequence (Copy_u8StepperId,Copy_u8Direction,Copy_u16StepDelay);
			}
		}
	}
	/*Function return*/
	return Local_u8Error;
}


/*
 * Description: Static Function to apply the sequence needed for the Active low stepper motor to rotate
 * Inputs: The stepper Number, the required direction, and time between each step
 * Output: void
 */
static void STPM_vSetActiveLowSequence (u8 Copy_u8StepperNo,u8 Copy_u8Direction,u8 Copy_u8StepTime)
{
	/*if the required direction is CW */
	if (Copy_u8Direction == STPM_u8_CW)
	{
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
	}
	/*if the required direction is CCW */
	else
	{
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
	}
	return;
}

/*
 * Description: Static Function to apply the sequence needed for the Active high stepper motor to rotate
 * Inputs: The stepper Number, the required direction, and time between each step
 * Output: void
 */
static void STPM_vSetActiveHighSequence (u8 Copy_u8StepperNo,u8 Copy_u8Direction,u8 Copy_u8StepTime)
{
	/*if the required direction is CW */
	if (Copy_u8Direction == STPM_u8_CW)
	{
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
	}
	/*if the required direction is CCW */
	else
	{
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_LOW);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].YellowPin,DIO_u8_PIN_LOW);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].OrangePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].BluePin,DIO_u8_PIN_HIGH);
		DIO_u8SetPinValue (STPM_ASu8StpmArray[Copy_u8StepperNo].PinkPin,DIO_u8_PIN_HIGH);
		delay_milliseconds(Copy_u8StepTime);
	}
	return;
}

