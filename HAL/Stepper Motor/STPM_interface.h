/**********************************************************/
/* Author: Mahmoud Alaa Elwelily                          */
/* Date: 16-Mar-19                                        */
/* Version: 01                                            */
/* Description: Interface file for Stepper Motor Driver   */
/**********************************************************/

/*Preprocessor Guard*/
#ifndef STPM_INTERFACE_H 
#define STPM_INTERFACE_H 

/*Macros to define the clockwise and anti clockwise directions*/
#define STPM_u8_CW     (u8)1
#define STPM_u8_CCW    (u8)0

/*Macros to define various speeds for the rotation of the stepper motor
 * Note: You can pass to the function another required time other than these macros*/
#define STPM_u16HIGH_SPEED        (u16)1100
#define STPM_u16MODERATE_SPEED    (u16)2000
#define STPM_u16LOW_SPEED         (u16)3000

/*
 * Description: Function to set the angle of the assigned stepper motor to a required angle within a desired time
 * Inputs: The stepper Number, the desired angle, the required direction, and the rotation desired time (speed)
 * Output: the error state
 */
u8 STPM_u8Rotate (u8 Copy_u8StepperId,u32 Copy_u8Angle,u8 Copy_u8Direction,u16 Copy_u16TimeInMs);

#endif /* STPM_INTERFACE_H */ 
